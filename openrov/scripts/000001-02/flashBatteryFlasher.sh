#!/bin/bash -e

if [ "$UID" != 0 ] ; then
    echo "$0: This script needs to be run as root" 1>&2
    exit 1
fi

firmware=$1

if [ "$firmware" == "" ] ; then
    firmware=/opt/openrov/firmware/000001-02/samd21/fuelgauge.bin
fi

# Select SAMD on the SWD switch
/opt/openrov/firmware/scripts/000001-02/selectSAMD.sh

# Flash firmware
result=1

echo "Flashing Battery/Fuel Gauge firmware with $firmware"

for ((try=0; try < 5; try++)) ; do
    if openocd -f /usr/share/openocd/scripts/board/openrov_trident_samd.cfg -c "program $firmware verify; reset; exit"
    then
        echo "Flash success"
        result=0
        break
    fi

    if [ $try -lt 4 ] ; then
        echo "Flash failure, will retry"
        sleep 2
    fi
done

echo "Flash failure, exiting"

exit $result
