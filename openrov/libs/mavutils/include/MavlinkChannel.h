#pragma once

#include <ringbuffer.h>
#include <periph/uart.h>
#include <mutex.h>

#include "MavMessageQueue.h"

struct TUartCallbackData
{
    uart_t dev;
	ringbuffer_t *buffer;
};

class MavlinkChannel
{
public:
	static constexpr int NO_MESSAGE_AVAILABLE = -1;
	mavlink_channel_t m_channel;

private:
	static constexpr int RX_BUFFER_SIZE = 512;

	// UART Parameters
	uart_t m_uartDev;
	uint32_t m_baudRate;
	uint8_t m_rxBuffer[ RX_BUFFER_SIZE ];
    ringbuffer_t m_rxRingBuffer;

	// Mavlink parameters
	mavlink_message_t* mp_currentMessage;

	mavlink_message_t m_parsedMessage;
	mavlink_status_t m_status;

	int m_currentMessageID;
	int m_badCRCs;	
	int m_droppedWhenFull;

	bool m_isAvailable;

	MavMessageQueue<MAVLINK_APP_MAX_MESSAGES_PER_CHANNEL> m_queue;

public:
	// Methods
	MavlinkChannel( uart_t uartDevIn, uint32_t baudRateIn, mavlink_channel_t channelIn );

	int Initialize();
	void Update();
	void Reset();

	uint8_t ReadByte();
	void WriteByte( uint8_t byteIn );
	
	void Write( const uint8_t* bufferIn, size_t lengthIn );

	mavlink_message_t* GetCurrentMessage();
	mavlink_status_t* GetChannelStatus();

	int GetCurrentMessageID();
	int GetDroppedWhenFullCount();
	int GetBadCRCCount();

	unsigned int GetBytesAvailable();

	// These functions clear the count upon read
	uint32_t GetTxByteCount()	{ auto temp = m_txBytes; m_txBytes = 0; return temp; }
	uint32_t GetRxByteCount()	{ auto temp = m_rxBytes; m_rxBytes = 0; return temp; }
	bool IsAvailable();

private:
	TUartCallbackData m_cbData;

	void Receive();

	uint32_t m_txBytes = 0;
	uint32_t m_rxBytes = 0;
};