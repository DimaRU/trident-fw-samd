#pragma once

// Turn off pedantic warnings/errors for mavlink includes
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <mavlink/v1.0/openrov/mavlink.h>
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

template<size_t N>
class MavMessageQueue
{
private:
    mavlink_message_t m_messages[ N ];
    unsigned int m_head 		= 0;
    unsigned int m_available 	= 0;

public:
    MavMessageQueue(){}

    bool IsEmpty()
    {
        return ( m_available == 0 );
    }

    bool IsFull()
    {
        // Number of available commands matches the size of the queue
        return ( m_available == N );
    }

    void Write( mavlink_message_t &messageIn )
    {
        // If there is no room or parsing the command fails, the message is dropped
        if( !IsFull() )
        {
            // Get the next free command location
            unsigned pos = m_head + m_available;

            if( pos >= N ) 
            {
                // Wrap around
                pos -= N;
            }

            // Copy the message to the queue
            m_messages[ pos ] = messageIn;
            m_available++;
        }
    }

    // Returns a pointer to the next message, if one exists, otherwise null
    mavlink_message_t* Read()
    {
        if( !IsEmpty() )
        {
            // Get pointer to the head element
            mavlink_message_t* t = &m_messages[ m_head ];

            // Move head forward and remove one available
            ++m_head;
            --m_available;

            // Handle head wrap-around
            if( m_head == N ) 
            {
                m_head = 0;
            }

            return t;
        }

        return nullptr;
    }

    void Reset()
    {
        m_available = 0;
    }
};