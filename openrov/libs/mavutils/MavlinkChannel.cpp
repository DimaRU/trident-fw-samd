#include "MavlinkChannel.h"
#include <log.h>
#include <xtimer.h>


void uart_recv_callback( void *arg, uint8_t c )
{
    TUartCallbackData *data = (TUartCallbackData*)arg;

    // Move byte from UART into the ringbuffer
    ringbuffer_add_one( data->buffer, (char)c );
}

uint8_t mavlink_parse_char_plus(uint8_t chan, uint8_t c, mavlink_message_t* r_message, mavlink_status_t* r_mavlink_status)
{
    uint8_t msg_received = mavlink_frame_char(chan, c, r_message, r_mavlink_status);
    if (msg_received == MAVLINK_FRAMING_BAD_CRC) {
	    // we got a bad CRC. Treat as a parse failure
	    mavlink_message_t* rxmsg = mavlink_get_channel_buffer(chan);
	    mavlink_status_t* status = mavlink_get_channel_status(chan);
	    status->parse_error++;
	    status->msg_received = MAVLINK_FRAMING_BAD_CRC;
	    status->parse_state = MAVLINK_PARSE_STATE_IDLE;
	    if (c == MAVLINK_STX)
	    {
		    status->parse_state = MAVLINK_PARSE_STATE_GOT_STX;
		    rxmsg->len = 0;
		    mavlink_start_checksum(rxmsg);
	    }
	    return MAVLINK_FRAMING_BAD_CRC;
    }
    return msg_received;
}

MavlinkChannel::MavlinkChannel( uart_t uartDevIn, uint32_t baudRateIn, mavlink_channel_t channelIn )
    : m_channel{ channelIn }
    , m_uartDev{ uartDevIn }
    , m_baudRate{ baudRateIn }
	, mp_currentMessage{ nullptr }
    , m_currentMessageID{ NO_MESSAGE_AVAILABLE }
    , m_badCRCs{ 0 }
    , m_droppedWhenFull{ 0 }
    , m_isAvailable{ false }
{
    m_cbData.dev = m_uartDev;
    m_cbData.buffer = &m_rxRingBuffer;
}

// Returns 0 for success, negative value for error code
int MavlinkChannel::Initialize()
{
    // Initialize the ringbuffer and clear its contents if used before
    ringbuffer_init( &m_rxRingBuffer, (char*)m_rxBuffer, RX_BUFFER_SIZE );

    // Initialize the uart device

    auto ret = uart_init( m_uartDev, m_baudRate, uart_recv_callback, (void *)&m_cbData );
    m_isAvailable = ( ret == 0 );

    return ret;
}

void MavlinkChannel::Reset()
{
    // Reset the ringbuffer
    ringbuffer_init( &m_rxRingBuffer, (char*)m_rxBuffer, RX_BUFFER_SIZE );

    // Resets the channel's internal state back to IDLE, resetting the parser
    mavlink_reset_channel_status( m_channel );
}

void MavlinkChannel::Update()
{
    // First check to see if there is an available command on the queue
    mp_currentMessage = m_queue.Read();

    if( mp_currentMessage != nullptr )
    {
        // Set current message ID
        m_currentMessageID = mp_currentMessage->msgid;

        // Command successfully read from queue.
        // There is guaranteed space in the queue now, so go ahead and parse the stream for a potential new command
        Receive();
    }
    else
    {
        // No commands found in queue, parse stream to see if new command is available
        Receive();

        // Attempt to read new message off of queue
        mp_currentMessage = m_queue.Read();

        // Set current message ID
        if( mp_currentMessage != nullptr )
        {
            m_currentMessageID = mp_currentMessage->msgid;
        }
        else
        {
            m_currentMessageID = NO_MESSAGE_AVAILABLE;
        }
    }
}

uint8_t MavlinkChannel::ReadByte()
{
    uint8_t byte = 0;

    NVIC_DisableIRQ(static_cast<IRQn_Type>( SERCOM0_IRQn + _sercom_id( uart_config[m_uartDev].dev ) ) );

    // NOTE: Only cast output to uint8_t when the ringbuffer is guaranteed to have data
    // Otherwise you'll get a -1 cast to uint8_t
    byte = (uint8_t)ringbuffer_get_one( &m_rxRingBuffer );

    NVIC_EnableIRQ( static_cast<IRQn_Type>( SERCOM0_IRQn + _sercom_id( uart_config[m_uartDev].dev ) ) );

    return byte;
}

void MavlinkChannel::WriteByte( uint8_t byteIn )
{
    uart_write( m_uartDev, &byteIn, 1 );
}

void MavlinkChannel::Write( const uint8_t* bufferIn, size_t lengthIn )
{
    m_txBytes += lengthIn;
    uart_write( m_uartDev, bufferIn, lengthIn );
}

mavlink_message_t* MavlinkChannel::GetCurrentMessage()
{
    return mp_currentMessage;
}

mavlink_status_t* MavlinkChannel::GetChannelStatus()
{
    return &m_status;
}

int MavlinkChannel::GetCurrentMessageID()
{
    return m_currentMessageID;
}

int MavlinkChannel::GetDroppedWhenFullCount()
{
    return m_droppedWhenFull;
}

int MavlinkChannel::GetBadCRCCount()
{
    return m_badCRCs;
}

bool MavlinkChannel::IsAvailable()
{
    return m_isAvailable;
}

unsigned int MavlinkChannel::GetBytesAvailable()
{
    return m_rxRingBuffer.avail;
}

void MavlinkChannel::Receive()
{
    auto count = m_rxRingBuffer.avail;
    m_rxBytes += count;

    while( count-- > 0 )
    {
        // Ringbuffer guaranteed to have data if we are in this loop
        uint8_t byte = ReadByte();

        // Attempt to parse
        uint8_t ret = mavlink_parse_char_plus( m_channel, byte, &m_parsedMessage, &m_status );

        // Try to get a new message
        if( ret == MAVLINK_FRAMING_OK )
        {
            // Handle message
            if( !m_queue.IsFull() )
            {
                m_queue.Write( m_parsedMessage );
            }
            else
            {
                m_droppedWhenFull++;
            }
        }
        else if( ret == MAVLINK_FRAMING_BAD_CRC )
        {
            // Parse failed due to bad CRC
            m_badCRCs++;            
        }
    }
}