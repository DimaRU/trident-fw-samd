#pragma once

// Includes
#include <cstddef>
#include <cstdint>
#include <cstring>



namespace orutil
{
    inline size_t strnlen(const char *str, size_t max)
    {
        const char *end = (const char*)memchr (str, 0, max);
        return end ? (size_t)(end - str) : max;
    }

    inline bool safecopy( char* destIn, const char* const sourceIn, size_t maxLengthIn )
    {
        if( strnlen( sourceIn, maxLengthIn ) == maxLengthIn )
        {
            // Do not perform copy if string is not properly null-terminated
            return false;
        }

        // Copy valid string
        strncpy( destIn, sourceIn, maxLengthIn );
        return true;
    }

    // Compile-time string literal equality check
    constexpr bool strings_equal( char const * a, char const * b ) 
    {
        return *a == *b && (*a == '\0' || strings_equal(a + 1, b + 1));
    }

    class CTimer
    {
    private:
        // Last time the timer was successfully polled for an elapsed amount of time
        uint32_t m_lastTime_us;		

    public:
        uint32_t Now();
        bool HasElapsed( uint32_t usIn );
        void Reset();
    };

    // To be used for driver method invocations
    // Example: 
    // uint8_t data[ 6 ];
    // IOResult<BNO055::TResult, i2c_retcode_t> result = BNO055::ReadVector( I2C_0, BNO055::ADDRESS_A, BNO055::VECTOR_EULER, data );
    template<typename TDriver, typename TComm>
    struct IOResult
    {
        TDriver driver;
        TComm   comm;
    };

    template<size_t n>
    struct TResultCount
    {
        template<typename T>
        T Add( T resultIn )
        {
            ++results[ static_cast<uint32_t>( resultIn ) ];
            return resultIn;
        }
        
        template<typename T>
        uint32_t GetCount( T resultIn )
        {
            return results[ static_cast<uint32_t>( resultIn ) ];
        }

        void ClearAll()
        {
            memset( results, 0, n * sizeof(uint32_t) );
        }

        template<typename T>
        void ClearResult( T resultIn )
        {
            results[ static_cast<uint32_t>( resultIn ) ] = 0;
        }

    private:
        uint32_t results[ n ] = { 0 };
    };

    ///////////////////////////////////////
    // Methods
    
    // Maps a floating point number with a specified input range to the specified output range
    constexpr float mapf( float x, float in_min, float in_max, float out_min, float out_max )
    {
        return ( (x - in_min ) * ( out_max - out_min ) / ( in_max - in_min ) + out_min );
    }
    
    // Normalizes an angle to between 0 and 360 degrees
    constexpr float NormalizeAngle360( float angle )
    {
        return ((angle > 360.0f) ? (angle - 360.0f) : ((angle < 0.0f) ? (angle + 360.0f) : angle));
    }

    // Normalizes an angle to between -180 and 180 degrees
    constexpr float NormalizeAngle180( float angle )
    {
        return ((angle > 180.0f) ? (angle - 360.0f) : ((angle < -180.0f) ? (angle + 360.0f) : angle));
    }
}