#pragma once

#include <periph/i2c.h>
#include <cstdint>
#include <orutil.h>

enum class EI2CState : uint8_t
{
    UNINITIALIZED,
    READY,
    FAILURE
};

class I2C
{
public:
    // Statistics
    orutil::TResultCount<I2C_RETCODE_COUNT> m_results;

    // Methods
    I2C( i2c_t i2cDevIn, i2c_speed_t speedIn );
    I2C(const I2C&) = delete;                   // Delete copy constructor
    I2C& operator=(const I2C &) = delete;       // Delete copy assignment operator

    i2c_retcode_t CheckSDA();               // Disables the interface
    i2c_retcode_t CheckSCL();               // Disables the interface
    i2c_retcode_t CheckForLatching();       // Disables the interface
    i2c_retcode_t PerformToggleRecovery();  // Disables the interface

    i2c_retcode_t CheckBusHealth();
    i2c_retcode_t Enable();
    i2c_retcode_t Reset();

    void Disable();

    bool Available(){ return m_state == EI2CState::READY; }
    EI2CState GetState(){ return m_state; }

    // Write operations
    i2c_retcode_t WriteByte(           uint8_t slaveAddressIn,                     uint8_t dataIn                            );
    i2c_retcode_t WriteRegisterByte(   uint8_t slaveAddressIn, uint8_t registerIn, uint8_t dataIn                            );
    i2c_retcode_t WriteBytes(          uint8_t slaveAddressIn,                     uint8_t *dataIn,    uint8_t numberBytesIn );
    i2c_retcode_t WriteRegisterBytes(  uint8_t slaveAddressIn, uint8_t registerIn, uint8_t *dataIn,    uint8_t numberBytesIn );

    // Read operations
    i2c_retcode_t ReadByte(            uint8_t slaveAddressIn,                     uint8_t *dataOut                          );
    i2c_retcode_t ReadBytes(           uint8_t slaveAddressIn,                     uint8_t *dataOut,   uint8_t numberBytesIn );
    i2c_retcode_t ReadRegisterByte(    uint8_t slaveAddressIn, uint8_t registerIn, uint8_t *dataOut                          );
    i2c_retcode_t ReadRegisterBytes(   uint8_t slaveAddressIn, uint8_t registerIn, uint8_t *dataOut,   uint8_t numberBytesIn );

private:
    const i2c_t m_i2cDev;
    const i2c_speed_t m_speed;

    uint32_t m_togglePeriod_us;

    gpio_t m_pinSDA;
    gpio_t m_pinSCL;

    EI2CState m_state = EI2CState::UNINITIALIZED;
};