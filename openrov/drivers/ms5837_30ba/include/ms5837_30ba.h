#pragma once

#include <i2cplus.h>
#include <orutil.h>

namespace ms5837_30ba
{
    enum class EOversampleRate : uint8_t
    {
        OSR_256_SAMPLES = 0,
        OSR_512_SAMPLES,
        OSR_1024_SAMPLES,
        OSR_2048_SAMPLES,
        OSR_4096_SAMPLES,
        OSR_8192_SAMPLES,

        _OSR_COUNT
    };

    struct TOversampleInfo
    {
        uint8_t commandMod;
        uint32_t conversionTime_us;
    };

    // I2C Slave Addresses
    namespace addresses
    {
        constexpr uint8_t A = 0x76;
        constexpr uint8_t B = 0x77;
    }

    // I2C Commands
    namespace commands
    {
        constexpr uint8_t ADC_READ          = 0x00;
        
        // CMD_RESET Note:
        //      The Reset sequence shall be sent once after power-on to make sure that the calibration PROM gets loaded into
        //      the internal register. It can be also used to reset the device PROM from an unknown condition.
        //      The reset can be sent at any time. In the event that there is not a successful power on reset this may be caused
        //      by the SDA being blocked by the module in the acknowledge state. The only way to get the MS5837-30BA to
        //      function is to send several SCLs followed by a reset sequence or to repeat power on reset.
        constexpr uint8_t RESET             = 0x1E;

        constexpr uint8_t PROM_READ_BASE    = 0xA0;         // Add coefficient index modifier
        constexpr uint8_t PRES_CONV_BASE    = 0x40;         // Add OSR modifier
        constexpr uint8_t TEMP_CONV_BASE    = 0x40 + 0x10;  // Add OSR modifier
    }

    // Computation constants
    namespace constants
    {
        constexpr int32_t POW_2_7  = 128;           // 2^7
        constexpr int32_t POW_2_8  = 256;           // 2^8
        constexpr int32_t POW_2_13 = 8192;          // 2^13
        constexpr int32_t POW_2_15 = 32768;         // 2^15
        constexpr int32_t POW_2_16 = 65536;         // 2^16
        constexpr int32_t POW_2_21 = 2097152;       // 2^21
        constexpr int32_t POW_2_23 = 8388608;       // 2^23
        constexpr int64_t POW_2_33 = 8589934592;    // 2^33
        constexpr int64_t POW_2_37 = 137438953472;  // 2^37

        // Table of command modifiers and conversion times for each OSR
        constexpr TOversampleInfo kOSRInfo[ (uint8_t)EOversampleRate::_OSR_COUNT ] =
        {
            // CMD, Delay(us)
            { 0x00, 1000 },    // 256: 1ms
            { 0x02, 2000 },    // 512: 2ms
            { 0x04, 3000 },    // 1024: 3ms
            { 0x06, 5000 },    // 2048: 5ms
            { 0x08, 10000 },   // 4096: 10ms
            { 0x0A, 20000 }    // 8192: 20ms
        };
    }
    
    // Globals used to efficiently perform conversion calculations
    // These should not be directly modified by user code
    namespace priv
    {
        extern int32_t     g_dT;        // Difference between actual and reference temperature
        extern int32_t     g_TEMP;      // Actual temperature
        
        extern int64_t     g_OFF;       // Offset at actual temperature
        extern int64_t     g_SENS;      // Sensitivity at actual temperature
        
        extern int64_t     g_Ti;        // Second order temperature component       
        extern int64_t     g_OFFi;      // Second order offset component            
        extern int64_t     g_SENSi;     // Second order sens component             
        
        extern int64_t     g_OFF2;      // Second order final off 
        extern int64_t     g_SENS2;     // Second order final sens
        extern int64_t     g_TEMP2;     // Second order final temp
        
        extern int32_t     g_P;         // Temperature compensated pressure
    }

    // Methods
    inline i2c_retcode_t Reset( I2C* dev, uint8_t slaveAddr )
    {
        return dev->WriteByte( slaveAddr, commands::RESET );
    }

    inline i2c_retcode_t ReadCalCoefficients( I2C* dev, uint8_t slaveAddr, uint16_t (&coefficientsOut)[8] )
    {
        uint8_t coeffs[ 2 ];

        // Read sensor coefficients
        for( uint8_t i = 0; i < 7; ++i )
        {
            i2c_retcode_t ret = dev->ReadRegisterBytes( slaveAddr, commands::PROM_READ_BASE + ( i * 2 ), coeffs, 2 );
            if( ret )
            {
                return ret;
            }

            // Combine high and low bytes
            coefficientsOut[i] = ( ( ( uint16_t )coeffs[ 0 ] << 8 ) | coeffs[ 1 ] );
        }

        return i2c_retcode_t::I2C_RETCODE_SUCCESS;
    }

    inline i2c_retcode_t StartTempConversion( I2C* dev, uint8_t slaveAddr, uint8_t commandModIn )
    {
        return dev->WriteByte( slaveAddr, commands::TEMP_CONV_BASE + commandModIn );
    }

    inline i2c_retcode_t StartPresConversion( I2C* dev, uint8_t slaveAddr, uint8_t commandModIn )
    {
        return dev->WriteByte( slaveAddr, commands::PRES_CONV_BASE + commandModIn );
    }

    inline i2c_retcode_t ReadRawConversion( I2C* dev, uint8_t slaveAddr, uint32_t &conversionResultOut )
    {
        uint8_t bytes[ 3 ];

        i2c_retcode_t ret = dev->ReadRegisterBytes( slaveAddr, commands::ADC_READ, bytes, 3 );
        if( ret )
        {
            return ret;
        }

        // Combine the bytes into one integer for the final result
        conversionResultOut = ( ( uint32_t )bytes[ 0 ] << 16 ) + ( ( uint32_t )bytes[ 1 ] << 8 ) + ( uint32_t )bytes[ 2 ];

        return i2c_retcode_t::I2C_RETCODE_SUCCESS;
    }

    inline bool CheckCRC4( uint16_t (&coefficientsIn)[8] )
    {
        // Get the CRC that was read from the device
        uint8_t readCRC = ( coefficientsIn[ 0 ] >> 12 );
        int cnt         = 0;
        uint32_t n_rem  = 0;
        uint8_t n_bit   = 0;
        
        // Replace the CRC byte with 0
        coefficientsIn[0] = ((coefficientsIn[0]) & 0x0FFF); 

        // Leftover value from the MS5803 series, set to 0 as specified by datasheet C code example for crc4
        coefficientsIn[7] = 0;
        
        // Loop through each byte in the coefficients
        for( cnt = 0; cnt < 16; ++cnt )
        { 
            // Choose LSB or MSB
            if( ( cnt % 2 ) == 1 ) 
            {
                n_rem ^= (uint16_t)( ( coefficientsIn[ cnt >> 1 ] ) & 0x00FF );
            }
            else
            {
                n_rem ^= (uint16_t)( coefficientsIn[ cnt >> 1 ] >> 8 );
            }
                
            for( n_bit = 8; n_bit > 0; --n_bit )
            {
                if( n_rem & 0x8000 )
                {
                    n_rem = ( n_rem << 1 ) ^ 0x3000;
                }
                else 
                {
                    n_rem = ( n_rem << 1 );
                }
            }
        }
        
        // Final 4-bit remainder is the CRC value
        n_rem = ( ( n_rem >> 12 ) & 0x000F ); 
        
        // Compare calculated result against read result
        return ( ( n_rem ^ 0x00 ) == readCRC );
    }

    inline void ProcessRawValues( uint16_t (&coefficientsIn)[8], uint32_t rawPressureIn, uint32_t rawTemperatureIn, float &pressureOut_mbar, float &temperatureOut_c )
    {
        using namespace priv;

        // Calculate base terms
        g_dT      = (int32_t)rawTemperatureIn - ( (int32_t)coefficientsIn[5] * constants::POW_2_8 );
        g_TEMP    = 2000 + ( ( (int64_t)g_dT * (int32_t)coefficientsIn[6] ) / constants::POW_2_23 );
        
        g_OFF     = ( (int64_t)coefficientsIn[2] * constants::POW_2_16 ) + ( ( (int64_t)coefficientsIn[4] * g_dT ) / constants::POW_2_7 );
        g_SENS    = ( (int64_t)coefficientsIn[1] * constants::POW_2_15 ) + ( ( (int64_t)coefficientsIn[3] * g_dT ) / constants::POW_2_8 );
        
        // Calculate intermediate values depending on temperature
        if( g_TEMP < 2000 )
        {
            // Temps < 20C
            g_Ti      = 3 * ( ( int64_t )g_dT * g_dT ) / constants::POW_2_33;
            g_OFFi    = 3 * ( ( g_TEMP - 2000 ) * ( g_TEMP - 2000 ) ) / 2 ;
            g_SENSi   = 5 * ( ( g_TEMP - 2000 ) * ( g_TEMP - 2000 ) ) / 8 ;
            
            // Additional compensation for very low temperatures (< -15C)
            if( g_TEMP < -1500 )
            {
                g_OFFi    = g_OFFi + 7 * ( ( g_TEMP + 1500 ) * ( g_TEMP + 1500 ) );
                g_SENSi   = g_SENSi + 4 * ( ( g_TEMP + 1500 ) * ( g_TEMP + 1500 ) );
            }
        }
        else
        {
            g_Ti      = 2 * ( ( int64_t )g_dT * g_dT ) / constants::POW_2_37;
            g_OFFi    = 1 * ( ( g_TEMP - 2000 ) * ( g_TEMP - 2000 ) ) / 16;
            g_SENSi   = 0;
        }
        
        g_OFF2    = g_OFF - g_OFFi;
        g_SENS2   = g_SENS - g_SENSi;
        
        g_TEMP2 = (g_TEMP - g_Ti);
        g_P = ( ( ( ( (int64_t)rawPressureIn * g_SENS2 ) / constants::POW_2_21 ) - g_OFF2 ) / constants::POW_2_13 );
        
        // Create data sample with calculated parameters
        temperatureOut_c = ( (float)g_TEMP2 / 100.0f );
        pressureOut_mbar = ( (float)g_P / 10.0f );
    }
}