// Includes
#include "ms5837_30ba.h"

namespace ms5837_30ba
{
    namespace priv
    {
        // Global instantiations
        int32_t     g_dT        = 0;
        int32_t     g_TEMP      = 0;
        
        int64_t     g_OFF       = 0;
        int64_t     g_SENS      = 0;
        
        int64_t     g_Ti        = 0; 
        int64_t     g_OFFi      = 0;         
        int64_t     g_SENSi     = 0;            
        
        int64_t     g_OFF2      = 0;
        int64_t     g_SENS2     = 0;
        int64_t     g_TEMP2     = 0;
        
        int32_t     g_P         = 0;
    }
}

