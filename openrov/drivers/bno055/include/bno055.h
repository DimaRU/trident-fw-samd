#pragma once

#include <i2cplus.h>
#include <orutil.h>
#include <log.h>
#include <inttypes.h>

#include <bno.h>
#include <bno055_quaternion.h>

namespace bno055 {
    // I2C Slave Addresses
    namespace addresses
    {
        constexpr uint8_t A = 0x28;
        constexpr uint8_t B = 0x29;
    }
    
    
    // Computation constants
    namespace constants
    {
       	constexpr uint8_t CHIP_ID                   = 0xA0;

        // Time delays
        constexpr uint32_t POR_RESET_DELAY_US       = 650000;   // Data sheet specifies "at least 650ms"
        constexpr uint32_t MODE_SWITCH_DELAY_US     = 30000;    // Data sheet specifies 19ms for N->Config and 7ms for Config->N. Allowing some leeway for both.
        constexpr uint32_t FUSION_UPDATE_PERIOD_US  = 10000;   // 100Hz in IMU, NDOF_FMC_OFF, and NDOF
        constexpr uint32_t GYRO_UPDATE_PERIOD_US    = 10000;   // 100Hz in fusion mode
    }

    
    

    // I2C Commands
    enum class ERegister : uint8_t
	{
		/* Page id register definition */
		PAGE_ID                                     = 0x07,

		/* PAGE0 REGISTER DEFINITION START*/
		CHIP_ID                                     = 0x00,
		ACCEL_REV_ID                                = 0x01,
		MAG_REV_ID                                  = 0x02,
		GYRO_REV_ID                                 = 0x03,
		SW_REV_ID_LSB                               = 0x04,
		SW_REV_ID_MSB                               = 0x05,
		BL_REV_ID                                   = 0x06,

		/* Accel data register */
		ACCEL_DATA_X_LSB                            = 0x08,
		ACCEL_DATA_X_MSB                            = 0x09,
		ACCEL_DATA_Y_LSB                            = 0x0A,
		ACCEL_DATA_Y_MSB                            = 0x0B,
		ACCEL_DATA_Z_LSB                            = 0x0C,
		ACCEL_DATA_Z_MSB                            = 0x0D,

		/* Mag data register */
		MAG_DATA_X_LSB                              = 0x0E,
		MAG_DATA_X_MSB                              = 0x0F,
		MAG_DATA_Y_LSB                              = 0x10,
		MAG_DATA_Y_MSB                              = 0x11,
		MAG_DATA_Z_LSB                              = 0x12,
		MAG_DATA_Z_MSB                              = 0x13,

		/* Gyro data registers */
		GYRO_DATA_X_LSB                             = 0x14,
		GYRO_DATA_X_MSB                             = 0x15,
		GYRO_DATA_Y_LSB                             = 0x16,
		GYRO_DATA_Y_MSB                             = 0x17,
		GYRO_DATA_Z_LSB                             = 0x18,
		GYRO_DATA_Z_MSB                             = 0x19,

		/* Euler data registers */
		EULER_H_LSB                                 = 0x1A,
		EULER_H_MSB                                 = 0x1B,
		EULER_R_LSB                                 = 0x1C,
		EULER_R_MSB                                 = 0x1D,
		EULER_P_LSB                                 = 0x1E,
		EULER_P_MSB                                 = 0x1F,

		/* Quaternion data registers */
		QUATERNION_DATA_W_LSB                       = 0x20,
		QUATERNION_DATA_W_MSB                       = 0x21,
		QUATERNION_DATA_X_LSB                       = 0x22,
		QUATERNION_DATA_X_MSB                       = 0x23,
		QUATERNION_DATA_Y_LSB                       = 0x24,
		QUATERNION_DATA_Y_MSB                       = 0x25,
		QUATERNION_DATA_Z_LSB                       = 0x26,
		QUATERNION_DATA_Z_MSB                       = 0x27,

		/* Linear acceleration data registers */
		LINEAR_ACCEL_DATA_X_LSB                     = 0x28,
		LINEAR_ACCEL_DATA_X_MSB                     = 0x29,
		LINEAR_ACCEL_DATA_Y_LSB                     = 0x2A,
		LINEAR_ACCEL_DATA_Y_MSB                     = 0x2B,
		LINEAR_ACCEL_DATA_Z_LSB                     = 0x2C,
		LINEAR_ACCEL_DATA_Z_MSB                     = 0x2D,

		/* Gravity data registers */
		GRAVITY_DATA_X_LSB                          = 0x2E,
		GRAVITY_DATA_X_MSB                          = 0x2F,
		GRAVITY_DATA_Y_LSB                          = 0x30,
		GRAVITY_DATA_Y_MSB                          = 0x31,
		GRAVITY_DATA_Z_LSB                          = 0x32,
		GRAVITY_DATA_Z_MSB                          = 0x33,

		/* Temperature data register */
		TEMP                                        = 0x34,

		/* Status registers */
		CALIB_STAT                                  = 0x35,
		SELFTEST_RESULT                             = 0x36,
		INTR_STAT                                   = 0x37,

		SYS_CLK_STATUS                              = 0x38,
		SYS_STATUS                                  = 0x39,
		SYS_ERR                                     = 0x3A,

		/* Unit selection register */
		UNIT_SEL                                    = 0x3B,
		DATA_SELECT                                 = 0x3C,

		/* Mode registers */
		OPR_MODE                                    = 0x3D,
		PWR_MODE                                    = 0x3E,

		SYS_TRIGGER                                 = 0x3F,
		TEMP_SOURCE                                 = 0x40,

		/* Axis remap registers */
		AXIS_MAP_CONFIG                             = 0x41,
		AXIS_MAP_SIGN                               = 0x42,

		// Other registers excluded because they are not used yet. See Bosch reference headers for additional registers.
	};

    enum class EPowerMode : uint8_t
	{
		NORMAL      = 0x00,
		LOWPOWER    = 0x01,
		SUSPEND     = 0x02
	};

    enum class EOpMode : uint8_t
	{
		/* Operation mode settings*/
		CONFIG          = 0X00,    // Configuration mode
		ACCONLY         = 0X01,    // Raw accel only
		MAGONLY         = 0X02,    // Raw mag only
		GYRONLY         = 0X03,    // Raw gyro only
		ACCMAG          = 0X04,    // Raw acc and mag
		ACCGYRO         = 0X05,    // Raw acc and gyro
		MAGGYRO         = 0X06,    // Raw mag and gyro
		AMG             = 0X07,    // Raw accel, mag, and gyro
		IMUPLUS         = 0X08,    // 6DOF relative fusion using accel and gyro
		COMPASS         = 0X09,    // 6DOF absolute fusion using the accel and mag
		M4G             = 0X0A,    // 6DOF relative using the magnetometer as a gyro
		NDOF_FMC_OFF    = 0X0B,    // 9DOF absolute fusion with slow magnetic calibration
		NDOF            = 0X0C     // 9DOF absolute fusion with fast magnetic calibration
	};

    enum class EVectorAddress : uint8_t
	{
		ACCELEROMETER = static_cast<uint8_t>( ERegister::ACCEL_DATA_X_LSB ),
		MAGNETOMETER  = static_cast<uint8_t>( ERegister::MAG_DATA_X_LSB ),
		GYROSCOPE     = static_cast<uint8_t>( ERegister::GYRO_DATA_X_LSB ),
		EULER         = static_cast<uint8_t>( ERegister::EULER_H_LSB ),
		LINEARACCEL   = static_cast<uint8_t>( ERegister::LINEAR_ACCEL_DATA_X_LSB ),
		GRAVITY       = static_cast<uint8_t>( ERegister::GRAVITY_DATA_X_LSB )
	};

	enum class ESystemStatus : uint8_t
	{
		IDLE 				= 0x00,		// System idle
		ERROR 				= 0x01,		// System error is present. Check SYS_ERR register
		INIT_PERIPHS 		= 0x02,		// Initializing peripherals
		INIT_SYSTEM         = 0x03,		// Initializing system
		SELFTEST 			= 0x04,		// Executing self-test
		ACTIVE_FUSION 		= 0x05,		// Fusion algorithm running
		ACTIVE_RAW 			= 0x06		// System running without algorithm
	};

	enum class ESystemError : uint8_t
	{
		NONE                = 0x00,		// No error
		PERIPH_INIT         = 0x01,		// Error initializing peripherals
		SYSTEM_INIT         = 0x02,		// Error initializing system
		SELFTEST            = 0x03,		// Self-test failed
		REGMAP_VAL_OOR 		= 0x04,		// Register value out of range
		REGMAP_OOR          = 0x05,		// Register address out of range
		REGMAP_WRITE_ERROR 	= 0x06,		// Register write error
		LOWPWR_UNAVAIL 		= 0x07,		// Low power mode not available for selected operation mode
		ACCEL_PWR_UNAVAIL 	= 0x08,		// Accelerometer power mode not available
		FUSION_CONFIG 		= 0x09,		// Fusion algorithm configuration error
		SENSOR_CONFIG 		= 0x0A		// Sensor configuration error

	};

    

    
    class BNO055 : public BNO {
        
public:
    BNO055(I2C* _dev, uint8_t _slaveAddr);
    
    i2c_retcode_t ReadSystemStatus( ESystemStatus &statusOut );
    
    i2c_retcode_t ReadCalibration( TCalibrationData &calDataOut );
    i2c_retcode_t ReadOperatingMode( EOpMode &opModeOut );
       

    i2c_retcode_t SetAxisConfiguration( uint8_t axisConfig, uint8_t signConfig );
    i2c_retcode_t SetUnitConfiguration( uint8_t configIn );    
    i2c_retcode_t SetOperatingMode( const EOpMode& modeIn );
    i2c_retcode_t VerifyChipPOST( bool &successfulPost );
    
    bno055::Vector<3>						m_rollPitchYaw055;        
        
    bno055::ESystemStatus 		            m_status;
    bno055::Quaternion 			            m_quatData055;
    BNO::TRawIMUData 	      	            m_gyroData;
    BNO::TCalibrationData   	            m_calData;
    

private:        
    // Private globals for intermediate calculations and data fetching 
    uint8_t g_tempBuffer[ 8 ];
        
	enum class EPage : uint8_t
	{
		PAGE_0 = 0x00,
		PAGE_1 = 0x01
	};
    
    // Trident tailboard correction
    const uint8_t m_axisMap 	= 0x21;
	const uint8_t m_axisSign 	= 0x02;

    // Set up units and orientation
    const uint8_t m_unitSelect =	( 0 << 7 ) |	// Orientation = Windows (-180 to 180 pitch)
                                    ( 0 << 4 ) |	// Temperature = Celsius
                                    ( 1 << 2 ) |	// Euler = Rads
                                    ( 1 << 1 ) |	// Gyro = Rads/s
                                    ( 0 << 0 );		// Accelerometer = m/s2
		
    
    struct TRevisionInfo
	{
		uint8_t  accel;
		uint8_t  mag;
		uint8_t  gyro;

		uint8_t  bootloader;
		uint16_t software;
	};


    // Methods
    i2c_retcode_t Reset( void );
    i2c_retcode_t Configure( void );
	i2c_retcode_t VerifyChipId( bool &validChip );
    i2c_retcode_t EnableAccel( void );

    
	i2c_retcode_t ReadSystemError( uint8_t &errorOut );

	i2c_retcode_t ReadQuaternion( BNOQuaternion &quatOut );
    
	i2c_retcode_t ReadGyro( TRawIMUData &dataOut );
	i2c_retcode_t SelectPage( const EPage& pageIn );
	i2c_retcode_t SetPowerMode( const EPowerMode& modeIn );

    
    i2c_retcode_t EnableRotationVector( void );
    i2c_retcode_t EnableGyro( void );
    i2c_retcode_t EnableMagnetometer( void);
    
    void SetNewAngVelMsgAvailable( bool newQuatMsg );
    void SetNewQuatMsgAvailable( bool newAngVelMsg );
    
    bool GetNewQuatMsgAvailable();
    bool GetNewAngVelMsgAvailable();
    
    i2c_retcode_t Calibrate( void );
    bool DataAvailable( void );
    
    float GetQuatI();
    float GetQuatJ();
    float GetQuatK();
    float GetQuatReal();    
        
    float GetGyroX();
    float GetGyroY();
    float GetGyroZ();
};
}



