#pragma once

#include <stdint.h>

enum class EWatchdogPeriod : uint8_t
{
    // Periods are roughly equal to the below values, in units of milliseconds 
    // Divide by 1024 and multiply by 1000 for more accurate ms times
    WDT_PERIOD_8 = 0x0,
    WDT_PERIOD_16 = 0x1,
    WDT_PERIOD_32 = 0x2,
    WDT_PERIOD_64 = 0x3,
    WDT_PERIOD_128 = 0x4,
    WDT_PERIOD_256 = 0x5,
    WDT_PERIOD_512 = 0x6,
    WDT_PERIOD_1024 = 0x7,
    WDT_PERIOD_2048 = 0x8,
    WDT_PERIOD_4096 = 0x9,
    WDT_PERIOD_8192 = 0xA,
    WDT_PERIOD_16384 = 0xB
};

class WatchdogTimer 
{
public:
    static volatile bool earlyWarningTrigger;
    static volatile int earlyWarningCount;

    WatchdogTimer();

    // Enable the watchdog timer to reset the machine after a period of time
    // without any calls to reset().  The passed in period (in milliseconds)
    // is just a suggestion and a lower value might be picked if the hardware
    // does not support the exact desired value.
    // User code should NOT set the 'isForSleep' argument either way --
    // it's used internally by the library, but your sketch should leave this
    // out when calling enable(), just let the default have its way.
    //
    // The actual period (in milliseconds) before a watchdog timer reset is
    // returned.
    int Enable( uint8_t timeoutPeriod, uint8_t earlyWarningPeriod );

    // Reset or 'kick' the watchdog timer to prevent a reset of the device.
    void Reset();

    // Completely disable the watchdog timer.
    void Disable();

private:
    void Initialize();

    bool m_initialized;
};