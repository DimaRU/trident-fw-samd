#pragma once
/*
 *  Notes:
 *  	Examples taken from SparkFun Arduino Example
 *  		https://github.com/sparkfun/SparkFun_BNO080_Arduino_Library
 */

namespace bno080
{
	// Channels
	namespace channels
	{
		constexpr uint8_t CHANNEL_COMMAND 			= 0;
		constexpr uint8_t CHANNEL_EXECUTABLE		= 1;
		constexpr uint8_t CHANNEL_CONTROL 			= 2;
		constexpr uint8_t CHANNEL_REPORTS			= 3;
		constexpr uint8_t CHANNEL_WAKE_REPORTS 		= 4;
		constexpr uint8_t CHANNEL_GYRO				= 5;
	}
	// All the ways we can configure or talk to the BNO080, figure 34, page 36 reference manual
	// These are used for low level communication with the sensor, on channel 2
	namespace shtp
	{
		constexpr uint8_t SHTP_REPORT_COMMAND_RESPONSE 			=	0xF1;
		constexpr uint8_t SHTP_REPORT_COMMAND_REQUEST	 	 	=	0xF2;
		constexpr uint8_t SHTP_REPORT_FRS_READ_RESPONSE 		=	0xF3;
		constexpr uint8_t SHTP_REPORT_FRS_READ_REQUEST 			=	0xF4;
		constexpr uint8_t SHTP_REPORT_PRODUCT_ID_RESPONSE 		=	0xF8;
		constexpr uint8_t SHTP_REPORT_PRODUCT_ID_REQUEST 		=	0xF9;
		constexpr uint8_t SHTP_REPORT_BASE_TIMESTAMP 			=	0xFB;
		constexpr uint8_t SHTP_REPORT_SET_FEATURE_COMMAND 		=	0xFD;
	}

	// All the different sensors and features we can get reports from
	// These are used when enabling a given sensor
	namespace reportIds
	{
		constexpr uint8_t SENSOR_REPORTID_ACCELEROMETER 				=	0x01;
		constexpr uint8_t SENSOR_REPORTID_GYROSCOPE 					=	0x02;
		constexpr uint8_t SENSOR_REPORTID_MAGNETIC_FIELD 				=	0x03;
		constexpr uint8_t SENSOR_REPORTID_LINEAR_ACCELERATION 			=	0x04;
		constexpr uint8_t SENSOR_REPORTID_ROTATION_VECTOR 				=	0x05;
		constexpr uint8_t SENSOR_REPORTID_GRAVITY 						=	0x06;
		constexpr uint8_t SENSOR_REPORTID_GAME_ROTATION_VECTOR 			=	0x08;
		constexpr uint8_t SENSOR_REPORTID_GEOMAGNETIC_ROTATION_VECTOR 	=	0x09;
		constexpr uint8_t SENSOR_REPORTID_TAP_DETECTOR 					=	0x10;
		constexpr uint8_t SENSOR_REPORTID_STEP_COUNTER 					=	0x11;
		constexpr uint8_t SENSOR_REPORTID_STABILITY_CLASSIFIER 			=	0x13;
		constexpr uint8_t SENSOR_REPORTID_PERSONAL_ACTIVITY_CLASSIFIER 	=	0x1E;
	}

	// Record IDs from figure 29, page 29 reference manual
	// These are used to read the metadata for each sensor type
	enum class ERecordIds : uint16_t
	{
		FRS_RECORDID_ACCELEROMETER 					=	0xE302,
		FRS_RECORDID_GYROSCOPE_CALIBRATED 			=	0xE306,
		FRS_RECORDID_MAGNETIC_FIELD_CALIBRATED 		=	0xE309,
		FRS_RECORDID_ROTATION_VECTOR 				=	0xE30B
	};

	// Command IDs from section 6.4, page 42
	// These are used to calibrate, initialize, set orientation, tare etc the sensor
	namespace commandIds
	{
		constexpr uint8_t COMMAND_ERRORS 			= 1;
		constexpr uint8_t COMMAND_COUNTER 			= 2;
		constexpr uint8_t COMMAND_TARE 				= 3;
		constexpr uint8_t COMMAND_INITIALIZE 		= 4;
		constexpr uint8_t COMMAND_DCD 				= 6;
		constexpr uint8_t COMMAND_ME_CALIBRATE 		= 7;
		constexpr uint8_t COMMAND_DCD_PERIOD_SAVE 	= 9;
		constexpr uint8_t COMMAND_OSCILLATOR 		= 10;
		constexpr uint8_t COMMAND_CLEAR_DCD 		= 11;
	}

	enum class ECalibrateIds
	{
		CALIBRATE_ACCEL 		 =	0,
		CALIBRATE_GYRO 			 =	1,
		CALIBRATE_MAG 			 =	2,
		CALIBRATE_PLANAR_ACCEL 	 =	3,
		CALIBRATE_ACCEL_GYRO_MAG =	4,
		CALIBRATE_STOP 			 =	5
	};

	namespace addresses{
		constexpr uint8_t A 			= 0x4A;
		constexpr uint8_t B 			= 0x4B;

		constexpr uint8_t DFU_0 = 0x28;		//device firmware upgrade, for upgrading bno080 firmware
		constexpr uint8_t DFU_1 = 0x29;

	}

	namespace constants
	{
		// Time delays - from BNO055, cannot find for 080
		constexpr uint32_t POR_RESET_DELAY_US 		= 650000;
		constexpr uint32_t MODE_SWITCH_DELAY_US 	= 30000;
		constexpr uint32_t FUSION_UPDATE_PERIOD_US  = 10000;   // 100Hz in IMU, NDOF_FMC_OFF, and NDOF
		constexpr uint32_t RESET_READ_DELAY_US		= 50000;	//50 ms per Sparkfun library, else device has been seen to reset

		// Packets can be up to 32k
		constexpr uint32_t MAX_PACKET_SIZE 		=	128;
		constexpr uint32_t MAX_METADATA_SIZE 	=	  9;
		constexpr uint32_t I2C_BUFFER_LENGTH 	=	 32;
		constexpr uint32_t HEADER_LEN 			= 	  4;
        
        constexpr uint32_t CONFIDENCES          =     9;
	}
}
