#pragma once

#include <orutil.h>
#include <assert.h>
#include <log.h>

enum class EI2CDeviceId : uint8_t
{
    BNO055 = 0,
    MPL3115,
    MS5837,
    PCA9538,
    BQ34Z100G1,

    COUNT
};

enum class EI2CEvent : uint8_t
{
    POWER_RESET = 0,  // To signal devices that they need to reset their current state because devices have been power cycled
    BUS_AVAILABLE,    // To signal devices that the i2c bus has been brought back up and should be usable again
    BUS_UNAVAILABLE   // To signal devices that the i2c bus has been lost and they shouldn't bother trying to use it for now  
};

// Callback method for I2C events
typedef void (*i2c_callback_t)( EI2CEvent event, const void* data );

class I2CDevice
{
public:
    I2CDevice( EI2CDeviceId idIn, const char* nameIn, I2C* i2cDevIn, uint32_t failureLimitIn, i2c_callback_t callbackIn, const void* callbackDataIn )
        : m_id{ idIn }
        , m_name{ nameIn }
        , m_i2c{ i2cDevIn }
        , m_failureLimit{ failureLimitIn }
        , m_callback{ callbackIn }
        , m_data{ callbackDataIn }
    {
    }

    void Callback( EI2CEvent eventIn )
    {
        if( m_callbackEnabled && ( m_callback != nullptr ) )
        {
            m_callback( eventIn, m_data );
        }
    }

    void EnableCallback()
    {
        m_callbackEnabled = true;
    }

    void DisableCallback()
    {
        m_callbackEnabled = false;
    }

    void UpdateFailures( bool failureOccurred )
    {
        if( failureOccurred )
        {
            ++m_consecutiveFailures;
        }
        else
        {
            m_consecutiveFailures = 0;
        }
    }

    void ResetFailures()
    {
        m_consecutiveFailures = 0;
    }

    uint32_t GetFailureCount()
    {
        return m_consecutiveFailures;
    }

    EI2CDeviceId GetId()
    {
        return m_id;
    }

    bool FailureLimitReached()
    {
        return ( m_consecutiveFailures >= m_failureLimit );
    }

    const char* GetName()
    {
        return m_name;
    }

    // String helpers
    static const char* EventToString( EI2CEvent eventIn )
    {
        switch( eventIn )
        {
            case EI2CEvent::POWER_RESET:     { return "POWER_RESET"; }
            case EI2CEvent::BUS_AVAILABLE:   { return "BUS_AVAILABLE"; }
            case EI2CEvent::BUS_UNAVAILABLE: { return "BUS_UNAVAILABLE"; }
            default: { assert( false ); return ""; }
        }
    }

private:
    const EI2CDeviceId      m_id;
    const char*             m_name;
    const I2C*              m_i2c;
    const uint32_t          m_failureLimit;
    const i2c_callback_t    m_callback;
    const void*             m_data;

    uint32_t                m_consecutiveFailures   = 0;
    bool                    m_callbackEnabled       = true;
};