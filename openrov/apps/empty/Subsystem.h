#pragma once

#include <orutil.h>
#include <assert.h>
#include <log.h>

#include "MavlinkBridgeHeader.h"

enum class EReservedSubstate : uint8_t
{
    RESET = 0,  // Subsystem should execute reset logic
    DELAY,      // Subsystem is delaying its transition to a new state

    // NOTE: All subsystem specific substate enums should start at EReservedSubstates::COUNT
    COUNT
};

struct TDelayedTransition
{
    // Attributes
    orutil::CTimer  timer;
    uint8_t         nextState;
    uint32_t        delay_us;

    // Methods
    uint8_t Next()
    {
        return nextState;
    }

    void Set( uint8_t nextStateIn, uint32_t delayIn )
    {
        timer.Reset();
        nextState = nextStateIn;
        delay_us = delayIn;
    }

    bool Ready()
    {
        return timer.HasElapsed( delay_us );
    }

    void Reset()
    {
        timer.Reset();
        nextState = 0;
        delay_us = 0;
    }
};

class Subsystem
{
public:
	Subsystem( OROV_TRIDENT_SUBSYSTEM_ID idIn, const char* nameIn ) 
        : m_name{ nameIn }
        , k_id{ idIn }
        , m_transition()
    {
        m_status.id = k_id;
        m_status.post_result = 0;
        m_status.state = 0;
        m_status.substate = 0;
        m_status.last_error_code = 0;
    }

    // Getters
	OROV_TRIDENT_SUBSYSTEM_ID GetId()                   { return static_cast<OROV_TRIDENT_SUBSYSTEM_ID>( k_id ); }
	OROV_SUBSYSTEM_STATE GetState()                     { return static_cast<OROV_SUBSYSTEM_STATE>( m_status.state ); }
    OROV_SUBSYSTEM_STATE GetStoredState()               { return static_cast<OROV_SUBSYSTEM_STATE>( m_storedState ); }
    OROV_POST_RESULT GetPostResult()                    { return static_cast<OROV_POST_RESULT>( m_status.post_result ); }
    const char* GetName()                               { return m_name; }
	uint8_t GetSubstate()                               { return m_status.substate; }          
	uint8_t GetStoredSubstate()                         { return m_storedSubstate; }
	uint8_t GetLastErrorCode()                          { return m_status.last_error_code; }
    mavlink_orov_subsystem_status_t* GetStatus()        { return &m_status; }

    // Setters
    void UpdateStatus( mavlink_message_t* msgIn )   { mavlink_msg_orov_subsystem_status_decode( msgIn, &m_status ); m_status.id = k_id; }

	void SetPostResult( OROV_POST_RESULT postResultIn ) { m_status.post_result = postResultIn; }

    template <typename T>
	void SetLastErrorCode( T errorCodeIn )
    { 
        m_status.last_error_code = static_cast<uint8_t>( errorCodeIn ); 
    }

    // Other helpers
    template <typename T>
	void TransitionState( T stateIn )
    {
        m_lastState = static_cast<OROV_SUBSYSTEM_STATE>( m_status.state );  // Record previous state
        m_status.state = static_cast<uint8_t>( stateIn );                   // Set new state
    }

    template <typename T>
	void TransitionSubstate( T substateIn )
    {
        m_lastSubstate      = m_status.substate;                    // Record previous substate
        m_status.substate   = static_cast<uint8_t>( substateIn );   // Set new substate
    }

    template <typename T>
	void TransitionSubstateDelayed( T substateIn, uint32_t delayIn_us )
    {
        TransitionSubstate( static_cast<uint8_t>( EReservedSubstate::DELAY ) );     // Move to the delayed substate
        m_transition.Set( static_cast<uint8_t>( substateIn ), delayIn_us );         // Set the delayed transition details
    }

	void StoreStateAndSubstate()
    {
        // Store the state and substate. Useful for entering STANDBY state and maintaining execution context.
        m_storedState       = static_cast<OROV_SUBSYSTEM_STATE>( m_status.state );       // Store previous state
        m_storedSubstate    = m_status.substate;    // Store previous state
    }

	void RestoreStateAndSubstate()
    {
        // Return to the previously stored state and substate. Useful for resuming where you left off from STANDBY state.
        if( m_storedState == OROV_SUBSYSTEM_STATE_STANDBY )
        {
            // Never return to standby, otherwise you get stuck. Go to recovery instead.
            TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
        }
        else
        {
            TransitionState( m_storedState );
        }
        
        TransitionSubstate( m_storedSubstate );
    }

    void TryDelayedTransition()
    { 
        // Check to see if transition is ready
        if( m_transition.Ready() )
        {
            TransitionSubstate( m_transition.Next() );
        } 
    }

    // Used for pretty printing debug information
    void Debug_ReportStatus()
    {
        // TODO: Re-implement
        // LOG_INFO( "Subsys: %s | post: %s | state: %s | substate: %d | error: %d\n",
        //     m_name,
        //     PostResultToString( (OROV_POST_RESULT)m_status.post_result ),
        //     SubsystemStateToString( (OROV_SUBSYSTEM_STATE)m_status.state ),
        //     m_status.substate,
        //     m_status.last_error_code );
    }
        
    // Mavlink Send Methods
    void SendStatus( mavlink_channel_t channelIn )
    {
        mavlink_msg_orov_subsystem_status_send_struct( channelIn, &m_status );
    }

private:
    mavlink_orov_subsystem_status_t m_status;
    const char* m_name;
    const uint8_t k_id;

    OROV_SUBSYSTEM_STATE m_lastState    = OROV_SUBSYSTEM_STATE_UNKNOWN;
    OROV_SUBSYSTEM_STATE m_storedState  = OROV_SUBSYSTEM_STATE_UNKNOWN;
    uint8_t m_lastSubstate              = static_cast<uint8_t>( EReservedSubstate::RESET );
	uint8_t m_storedSubstate            = static_cast<uint8_t>( EReservedSubstate::RESET );

	TDelayedTransition m_transition;
};
