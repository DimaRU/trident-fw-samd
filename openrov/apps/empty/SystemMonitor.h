#pragma once

#include <xtimer.h>
#include <orutil.h>
#include <samd21_wdt.h>

#include "MCU.h"
#include "Subsystem.h"

// TODO: Add this back in via mavlink
#define TRIDENT_MCU_COUNT (4)

// Includes
class SystemMonitor
{
// Static attributes and methods
// TODO: Could be better structured to communicate via RTOS messaging or, even better, using the Active Object architectural pattern
public:
    static void ResetWDT();
    static void RegisterSubsystem( Subsystem* subsystemIn );

private:
    static WatchdogTimer m_wdt;
    static Subsystem* mp_subsystems[ OROV_TRIDENT_SUBSYSTEM_ID::OROV_TRIDENT_SUBSYSTEM_ID_COUNT ];

public:
    SystemMonitor();

    void Initialize();
    void Update();

private:
    // SAMD21 MCU Instance
    MCU m_mcu;

    timex_t m_currentTime;
	orutil::CTimer m_timer_1hz;
    orutil::CTimer m_timer_10hz;

    mavlink_orov_wdt_warning_t m_wdtWarning;
    mavlink_orov_ping_t m_ping;

    // System methods
    void HandleMessages();
    void ReportMCUStatus();
    void ReportSubsystemStatus();

    void CheckResetCause();
};