#pragma once

// Includes
#include <mpl3115.h>
#include "I2CMonitor.h"
#include "Subsystem.h"

class Barometer : I2CDevice
{
public:
	enum class ESubstate : uint8_t
    {
        REINITIALIZING_DEVICE = static_cast<uint8_t>( EReservedSubstate::COUNT ),
        STARTING_MEASUREMENT,
        WAITING_FOR_DATA
    };

	enum class ERetcode : uint8_t
	{
		SUCCESS = 0,
		NO_DATA_AVAILABLE,
		ERROR_IO,
        ERROR_BAD_CHIP_ID,
		ERROR_BAD_STATE
	};

	// Methods
	explicit Barometer();

	void Initialize();
	void Post();
	void Update();

private:
	orutil::CTimer m_statusTimer;
	const uint32_t m_kStatusTimerPeriod_us = 1000000;

	mavlink_orov_barometer_reading_t m_data;

    uint8_t m_status = 0;
    uint8_t m_dataBuffer[ 5 ];

    // Config for 128 samples per reading (~half second updates)
    mpl3115::EOversampleRate m_osr  = mpl3115::EOversampleRate::OSR_128_SAMPLES;
	uint32_t m_sampleReadDelay_us	= mpl3115::constant::kOSRInfo[ (int)mpl3115::EOversampleRate::OSR_128_SAMPLES ];

	uint8_t m_postAttempts = 0;

	float m_temperature_c;
	float m_pressure_mbar;

	// Sensor methods
	ERetcode Reset();
    ERetcode CheckChipID();
    ERetcode ConfigureBarometer();
    ERetcode StartMeasurement();
    ERetcode CheckDataReady();
    ERetcode ReadMeasurement();
	ERetcode CheckHealth();

	// State methods
	bool PostAttempt();
	void HandleMessages();

	void Update_Active();
	void Update_Standby();
	void Update_Recovery();
	void Update_Disabled();
};
