/*
 *  Notes:
 *      Examples taken from SparkFun Arduino Example
 *          https://github.com/sparkfun/SparkFun_BNO080_Arduino_Library
 */
#include "IMU.h"
#include "NState.h"

#include "SystemMonitor.h"
#include "CommLink.h"

#include <math.h>

IMU::IMU()   
    : m_use055(false)
    , m_postAttempts(0)  // Looks like this is not used anywhere
    , m_opmodeWaitCounter(0)
{    
    m_device055 = new bno055::BNO055(&I2CMonitor::I2CMain, bno055::addresses::A);
    m_device080 = new bno080::BNO080(&I2CMonitor::I2CMain, bno080::addresses::A);
        
    m_i2cDevice055 = new I2CDevice(EI2CDeviceId::BNO055, OROV_TRIDENT_SUBSYSTEM_ID_IMU, "IMU_", 20);
    m_i2cDevice080 = new I2CDevice(EI2CDeviceId::BNO080, OROV_TRIDENT_SUBSYSTEM_ID_IMU, "IMU_", 30);
    
    SetDevice(false); // Initial setting
}

/**
 * Use MAVLink debug channel 
 * 
 * Messages are printed in trident-core debug output.  They are not sent to DDS.
 */

void IMU::SendDebug(uint32_t time, uint8_t ind, float value) {
    (void)time;
    (void)ind;
    (void)value;
    mavlink_msg_debug_send(CommLink::MAIN_CHANNEL, time, ind, value);
}


void sendDebug(uint32_t time, uint8_t ind, float value) {
    IMU::SendDebug(time, ind, value);
}

void IMU::UpdateFailures( bool failureOccured ) {
     
    if (failureOccured) {
        if (m_use055) {
            m_i2cDevice055->UpdateFailures( true );   
        } else {
            m_i2cDevice080->UpdateFailures( true );
        }
        
    } else {
        m_i2cDevice055->UpdateFailures( false );  
        m_i2cDevice080->UpdateFailures( false );
    }
}

void IMU::SetDevice(bool use055) {
    m_use055 = use055;
        
    if (m_use055) {
        m_i2cDevice = m_i2cDevice055;
        m_device    = m_device055;
    } else {
        m_i2cDevice = m_i2cDevice080;
        m_device    = m_device080;         
    }    
    
    UpdateFailures( false );
    
    SendDebug(0, 10, 6000);
    
    SystemMonitor::RegisterSubsystem( &m_i2cDevice->m_subsystem );
    I2CMonitor::RegisterDevice( m_i2cDevice );
    
    SendDebug(200, 2, m_use055 ? 55 : 80);
}

void IMU::Initialize()
{
    // Register with SystemMonitor
    SystemMonitor::RegisterSubsystem( &m_i2cDevice->m_subsystem );

    // Register I2C device with monitor
    I2CMonitor::RegisterDevice( m_i2cDevice );

    m_statusTimer.Reset();
    m_dataTimer.Reset();
    m_resetTimer.Reset();

    // Update state
    m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
    m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
}

void IMU::Post()
{
    // Reset state and inform that the post result is being awaited
    m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING);
    m_i2cDevice->m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

    // Check I2C availability
    if( m_i2cDevice->m_i2c->GetState() != EI2CState::READY )
    {
        
        // Set error code and result
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        m_i2cDevice->m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

        // Update state
        m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
        m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
        return;
    }

    int postCount     = 0;
    bool postResult = false;

    SendDebug(100, 1, 3);
    
    do
    {
        // Reset the WDT on each attempt
        SystemMonitor::ResetWDT();
                
        // Make an attempt to post
        postResult = PostAttempt();
        postCount++;
        SendDebug(1000, 1, postResult);
        SendDebug(1000, 1, postCount);
    }
    while( !postResult && postCount < 3 );

    if( postResult )
    {   // Success
        m_i2cDevice->m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );

        // Update state
        m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
        m_i2cDevice->m_subsystem.TransitionSubstate( ESubstate::ACTIVE );
    }
    else
    {   // Failure
        m_i2cDevice->m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

        // Update state
        m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
        m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
    }
}

bool IMU::PostAttempt()
{
    LOG_DEBUG( "IMU POST!\n" );

    SendDebug(100, 1, 4);
    
    // Reset the device
    auto ret = Reset();
    if( ret != ERetcode::SUCCESS ) { 
        SendDebug(0, 1, 4.05);
        return false;         
    }

    // Wait 100ms
    xtimer_usleep( m_use055 ?
        bno055::constants::POR_RESET_DELAY_US :
        bno080::constants::POR_RESET_DELAY_US ); // need constants for bno080

    ret = VerifyPostReset();
    SendDebug(0, 1, 4.15);
    if( ret != ERetcode::SUCCESS )
    { 
        SendDebug(0, 1, 4.1);
        return false;         
    }

    ret = Configure();
    if( ret != ERetcode::SUCCESS ){ 
        SendDebug(0, 1, 4.2);
        return false;         
    }
    SendDebug(100, 1, 4.3);

    xtimer_usleep( m_use055 ?
        bno055::constants::MODE_SWITCH_DELAY_US :
        bno080::constants::MODE_SWITCH_DELAY_US ); // needs to be changed for 80

    if (m_use055) {
        ret = CheckStatus( bno055::ESystemStatus::ACTIVE_FUSION );
        if( ret != ERetcode::SUCCESS ){ 
            //SendDebug(100, 1, 4.4);
            return false;             
        }  
    }
    
    LOG_DEBUG( "IMU SUCCESS!\n" );
    SendDebug(100, 1, 4.5);
    
    return true;
}

IMU::ERetcode IMU::Reset()
{
    if (!m_device) return ERetcode::ERROR_INCORRECT_STATE;
    
    m_statusTimer.Reset();
    m_dataTimer.Reset();
    
    if (m_firstReset || m_use055) {
        SendDebug(100, 1, 7.1);
        auto ret = m_device080->Reset( );    
        if (ret) {
            SendDebug(100, 1, 7.2);
            ret = m_device055->Reset( );            
        }
    
        if( ret )
        {
            // Fail
            m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
            UpdateFailures( true );
            return ERetcode::ERROR_IO;
        }
    
    
    } else {
         SendDebug(100, 1, 7.3);
         I2CMonitor::doHardReset = true;
        
    }
    
    SendDebug(100, 1, 7.4);

    UpdateFailures( false );    
    
    return ERetcode::SUCCESS;
}

IMU::ERetcode IMU::VerifyPostReset()
{    
    if (!m_device) return ERetcode::ERROR_INCORRECT_STATE;
    
    bool success = false;

    // Verify chip ID
    
    // There are a number of problems here.  For unknown reasons, after reset, the 085 sends
    // a bunch of data, which is not obvious how to parse.  The documentation suggests it sends
    // some data unsolicited.  
    //
    // In pratice what happens with our parsing and packet handling is that we issue the reset
    // twice and get an ACK in the first byte of the second packet (would have nominally
    // expected in the 4th byte), hence the debug and checks you see below.  
    //
    // In addition, issuing reset later doesn't seem effectual, hence the full i2c device
    // reset we trigger for the non-responsive case. 
    
    
    i2c_retcode_t ret;
    
    uint8_t id[32]; 
        
    SendDebug(0, 1, 3000); 
     
    ret = static_cast<bno080::BNO080 *>(m_device080)->VerifyChipIdGet(id);
    
    for (size_t byte = 0; byte < 32; byte++) { 
        if (id[byte] == 0xff) break;            
            SendDebug((id[byte] == 0xf8) ? 8888 : 2000, byte, /*2000 + */id[byte]);
            SendDebug(8888, byte, id[byte+1]);
        if (id[byte] == 0xf8) { 
            success = true;
            break;
        }            
    }
    
    if (ret == I2C_RETCODE_SUCCESS) {
        UpdateFailures( false );
        SendDebug(0, 1, 6.05);
        
        if (success) {
            SendDebug(0, 1, 6.06);
            SetDevice(false);
        }
        
    } else {
        success = false;
        
        SendDebug(0, 1, 16.1 + success); 
        ret = m_device055->VerifyChipId( success );
        
        if (ret == I2C_RETCODE_SUCCESS) {
            UpdateFailures( false );
            
            if (success) {
                SendDebug(0, 1, 6.15);
                SetDevice(true);
            }
        }
    }
        
    if( ret )
    {
        SendDebug(0, 1, 6.2);
        // Fail
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
    }
    else
    {
        if( !success )
        {
            SendDebug(0, 1, 6.3);
            
            m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_INVALID_ID );
            UpdateFailures( true );
            return ERetcode::ERROR_INVALID_ID;
        }
    }
    
    if (m_use055) {
        // Check post result
        ret = static_cast<bno055::BNO055 *>(m_device)->VerifyChipPOST(success );
        if( ret )
        {
            // Fail
            m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
            UpdateFailures( true );
            return ERetcode::ERROR_IO;
        }
        else
        {
            if( !success )
            {
                m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_FAILED_POST );
                UpdateFailures( true );
                return ERetcode::ERROR_FAILED_POST;
            }
        }

        // Check make sure we are in config mode
        bno055::EOpMode mode;
        ret = static_cast<bno055::BNO055 *>(m_device)->ReadOperatingMode( mode );
        if( ret )
        {
            // Fail
            m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
            UpdateFailures( true );
            return ERetcode::ERROR_IO;
        }
        else
        {
            if( mode != bno055::EOpMode::CONFIG )
            {
                m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_INCORRECT_STATE );
                UpdateFailures( true );
                return ERetcode::ERROR_INCORRECT_STATE;
            }
        }
    }
 
    SendDebug(0, 1, 6.4);
 
    UpdateFailures( false );
    return ERetcode::SUCCESS;        
}

IMU::ERetcode IMU::Configure()
{
    if (!m_device) return ERetcode::ERROR_INCORRECT_STATE;
    
    auto ret = m_device->Configure();
        
    if (ret != I2C_RETCODE_SUCCESS) {
        // Fail
        SendDebug(0, 0, 10001);
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
            
    } else {
        SendDebug(0, 0, 10002);
        UpdateFailures( false ); 
    }
    return ERetcode::SUCCESS;        
}

void IMU::Update()
{
    switch( m_i2cDevice->m_subsystem.GetState() )
    {
        case OROV_SUBSYSTEM_STATE_ACTIVE:     { Update_Active();         break; }
        case OROV_SUBSYSTEM_STATE_RECOVERY:   { Update_Recovery();       break; }
        case OROV_SUBSYSTEM_STATE_STANDBY:    { Update_Standby();        break; }
        case OROV_SUBSYSTEM_STATE_DISABLED:   { break; }

        default:
        {    // Move to the recovery state. These states aren't supported
            m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
            m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
            break;
        }
    }
}

void IMU::Update_Active()
{
    if (!m_device) return;
    
    switch( m_i2cDevice->m_subsystem.GetSubstate() )
    {
        case static_cast<uint8_t>( EReservedSubstate::DELAY ):
        {
            m_i2cDevice->m_subsystem.TryDelayedTransition();
            break;
        }

        case static_cast<uint8_t>( ESubstate::ACTIVE ):
        {
            //SendDebug(300, 1, 100 + m_use055);
            
            if (m_use055) {
                // TODO: Not sure this is correct. This will always be elapsed after the initial expiry
                if( m_dataTimer.HasElapsed( bno055::constants::FUSION_UPDATE_PERIOD_US ) )
                {
                    // Read quaternion data
                    if( ReadAttitudeData055() == ERetcode::SUCCESS )
                    {
                    // Send out updates
                        mavlink_msg_orov_imu_attitude_quaternion_send_struct( CommLink::MAIN_CHANNEL, &m_quat );
                        mavlink_msg_orov_imu_attitude_euler_send_struct( CommLink::MAIN_CHANNEL, &m_euler );
                    }
                }

                if( m_statusTimer.HasElapsed( m_kStatusTimerPeriod_us ) )
                {
                    // Check Health
                    if( CheckHealth() != ERetcode::SUCCESS )
                    {
                        // Go to recovery mode and restart the sensor
                        m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
                        m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
                    }

                    // Read sensor calibration
                    if( ReadCalibration() == ERetcode::SUCCESS )
                    {
                        // Send data
                        mavlink_msg_orov_imu_calibration_send_struct( CommLink::MAIN_CHANNEL, &m_calib );
                    }
                } 
                
            } else {                
#if 0
                // Test soft reset after 30 seconds
                if( m_resetTimer.HasElapsed( 30 * 1000 * 1000 ) ) {
                    SendDebug(100, 1, 9999);
                    m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
                    m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
                    m_resetTimer.Reset();          

                    I2CMonitor::doHardReset = true;
                
                } else 
#endif                    
                // 5 seconds of now update, reset
                if( m_statusTimer.HasElapsed( m_kStatusTimerPeriod_us * 5 ) ) {
                    SendDebug(1111, 1, 9000+(m_kStatusTimerPeriod_us * 5));
                    // Go to recovery mode and restart the sensor
                    I2CMonitor::doHardReset = true;

                    m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
                    m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
                    m_statusTimer.Reset();

                } else {
                    if( m_dataTimer.HasElapsed( bno080::constants::FUSION_UPDATE_PERIOD_US ) )
                    {
                        if( m_device->DataAvailable() )
                        {                                                
                            // Read quaternion data
                            if( ReadAttitudeData080() == ERetcode::SUCCESS )
                            {
                                //SendDebug(100, 1, 2);
                            
                                // Send out updates
                                mavlink_msg_orov_imu_attitude_quaternion_send_struct( CommLink::MAIN_CHANNEL, &m_quat );
                                mavlink_msg_orov_imu_attitude_euler_send_struct( CommLink::MAIN_CHANNEL, &m_euler );
                            
                                m_statusTimer.Reset();
                            }
                        } else {
                            //SendDebug(100, 1, 2); 
                        }
                    }
                }
            }

            break;
        }

        default:
        {   // Shouldn't get here
            m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
            m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
            break;
        }
    }
}

void IMU::Update_Recovery()
{
    switch( m_i2cDevice->m_subsystem.GetSubstate() )
    {
        case static_cast<uint8_t>( EReservedSubstate::DELAY ):
        {
            m_i2cDevice->m_subsystem.TryDelayedTransition();
            break;
        }

        case static_cast<uint8_t>( EReservedSubstate::RESET ):
        {
            SendDebug(100, 1, 8.05);
            // Check that the chip is responsive with a simple command
            ERetcode ret =  ERetcode::SUCCESS;

            if( ret != ERetcode::SUCCESS )
            {
                SendDebug(100, 1, 8.051);
                m_i2cDevice->m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 3000000 );
            }
            else
            {
                SendDebug(100, 1, 8.052);
                m_i2cDevice->m_subsystem.TransitionSubstateDelayed( ESubstate::POST_RESET, 
                                                       m_use055 ?
                                                       bno055::constants::POR_RESET_DELAY_US :
                                                       bno080::constants::POR_RESET_DELAY_US );
            }

            break;
        }

        case static_cast<uint8_t>( ESubstate::POST_RESET ):
        {
            // Check that the chip is responsive with a simple command
            ERetcode ret = VerifyPostReset();
            SendDebug(100, 1, 8.1);
            if( ret != ERetcode::SUCCESS )
            { 
                SendDebug(100, 1, 8.11);
                m_i2cDevice->m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 100000 );
            }
            else
            {
                SendDebug(100, 1, 8.12);
                m_i2cDevice->m_subsystem.TransitionSubstate( ESubstate::CONFIGURING_CHIP );
            }

            break;
        }

        case static_cast<uint8_t>( ESubstate::CONFIGURING_CHIP ):
        {
            // Check that the chip is responsive with a simple command
            SendDebug(100, 1, 8.2);
            ERetcode ret = Configure();

            if( ret != ERetcode::SUCCESS )
            {
                SendDebug(100, 1, 8.3);
                m_i2cDevice->m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 100000 );
            }
            else
            {
                m_i2cDevice->m_subsystem.TransitionSubstateDelayed( ESubstate::WAITING_FOR_ACTIVE, 
                                                       m_use055 ?
                                                       bno055::constants::MODE_SWITCH_DELAY_US :
                                                       bno080::constants::MODE_SWITCH_DELAY_US );
            }

            break;
        }

        case static_cast<uint8_t>( ESubstate::WAITING_FOR_ACTIVE ):
        {
            m_opmodeWaitCounter += 1;
            SendDebug(100, 1, 8.5);

            if( m_opmodeWaitCounter > 3 )
            {
                SendDebug(100, 1, 8.6);
                m_i2cDevice->m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 100000 );
            }
            
            if (m_use055) {
                // Check that the chip is responsive with a simple command
                ERetcode ret = CheckStatus( bno055::ESystemStatus::ACTIVE_FUSION );
                if( ret != ERetcode::SUCCESS )
                {
                    m_i2cDevice->m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 100000 );
                }
                else
                {
                    m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
                    m_i2cDevice->m_subsystem.TransitionSubstate( ESubstate::ACTIVE );
                }                
            }            

            break;
        }

        default:
        {
            // Shouldn't get here
            m_i2cDevice->m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
            m_i2cDevice->m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
            break;
        }
    }
}

void IMU::Update_Standby()
{
    // Check to see if I2C bus is back
    if( m_i2cDevice->m_i2c->Available() )
    {
        // Restore the original state
        m_i2cDevice->m_subsystem.RestoreStateAndSubstate();
    }
}

// BNO055 only - needs to be subclassed

IMU::ERetcode IMU::CheckStatus( const bno055::ESystemStatus &statusIn )
{
    // Check post result
    auto device = static_cast<bno055::BNO055 *>(m_device);
    
    auto ret = device->ReadSystemStatus(device->m_status);
    
    if( ret )
    {
        // Fail
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
    }
    else
    {
        if( device->m_status != statusIn )
        {
            m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_INCORRECT_STATE );
            UpdateFailures( true );
            return ERetcode::ERROR_INCORRECT_STATE;
        }
    }

    UpdateFailures( false );
    return ERetcode::SUCCESS;
}


// Data Getters---------------------------------------------------------

IMU::ERetcode IMU::ReadAttitudeData055()
{
    auto device = static_cast<bno055::BNO055 *>(m_device);
    
    // Read quaternion
    if( m_device->ReadQuaternion( device->m_quatData055 ) )
    {
        // Fail
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
    }

    // Read quaternion
    if( m_device->ReadGyro( device->m_gyroData ) )
    {
        // Fail
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
    }

    // Something is a bit odd here. This order works, although, when reading in trident-core
    // X = Q4, Y = Q1, Z = Q2, W = Q3.
    // If we use that order, the yaw indicator in Cockpit is backwards. Can't figure out what is going on.
    // This might actually be an error in Cockpit...
    m_quat.q1 = device->m_quatData055.w();
    m_quat.q2 = device->m_quatData055.x();
    m_quat.q3 = device->m_quatData055.y();
    m_quat.q4 = device->m_quatData055.z();

    m_quat.roll_rate     = device->m_gyroData.y;
    m_quat.pitch_rate    = device->m_gyroData.x;
    m_quat.yaw_rate      = device->m_gyroData.z;

    m_euler.roll_rate    = device->m_gyroData.y;
    m_euler.pitch_rate   = device->m_gyroData.x;
    m_euler.yaw_rate     = device->m_gyroData.z;

    // Calculate roll pitch yaw attitude in degrees
    CalculateRollPitchYaw();


    // Calculate compass heading
    CalculateCompassHeading();

    UpdateFailures( false );
    return ERetcode::SUCCESS;
}

// 055 only
IMU::ERetcode IMU::ReadCalibration()
{
     auto device = static_cast<bno055::BNO055 *>(m_device);
    
    // Read calibration data
    if(device->ReadCalibration( device->m_calData ) )
    {
        // Fail
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
    }

    // Update values
    m_calib.accel  = device->m_calData.accel;
    m_calib.gyro   = device->m_calData.gyro;
    m_calib.mag    = device->m_calData.mag;
    m_calib.system = device->m_calData.sys;

    UpdateFailures( false );
    return ERetcode::SUCCESS;
}

// 055 only
IMU::ERetcode IMU::CheckHealth()
{
    // Make sure we are still in NDOF mode
    bno055::EOpMode mode;
    auto ret = static_cast<bno055::BNO055 *>(m_device)->ReadOperatingMode( mode );
    if( ret )
    {
        // Fail
        m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
        UpdateFailures( true );
        return ERetcode::ERROR_IO;
    }
    else
    {
        if( mode != bno055::EOpMode::NDOF )
        {
            m_i2cDevice->m_subsystem.SetLastErrorCode( ERetcode::ERROR_INCORRECT_STATE );
            UpdateFailures( false );
            return ERetcode::ERROR_INCORRECT_STATE;
        }
    }
    
    UpdateFailures( false );
    return ERetcode::SUCCESS;
}

IMU::ERetcode IMU::ReadAttitudeData080()
{
	bno080::BNO080 *device = (bno080::BNO080 *)m_device;

	// TODO - IMU->NWU rotation -- Current IMU is 180 degrees around z from body frame. i.e. x is sternward
	device->m_quat_imu_nwu.w() = m_device->GetQuatReal();
	device->m_quat_imu_nwu.x() = m_device->GetQuatI();
	device->m_quat_imu_nwu.y() = m_device->GetQuatJ();
	device->m_quat_imu_nwu.z() = m_device->GetQuatK();

	// get rotation from NWU-->vehicle body -- not doing the transform yet TODO this is nwu at the moment
	device->m_quat_body_nwu =  device->m_quat_imu_nwu * device->m_quat_z_180rot;// * m_quat_body_imu;//m_quat_z_180rot;

	// Update mavlink msg with most recent IMU information. Mapping to message as follows
	m_quat.q4 = device->m_quat_body_nwu.x();
	m_quat.q1 = device->m_quat_body_nwu.y();
	m_quat.q2 = device->m_quat_body_nwu.z();
	m_quat.q3 = device->m_quat_body_nwu.w();

	// Get angular velocity as measured by IMU
	device->m_ang_vel_imu.x() = m_device->GetGyroX();
	device->m_ang_vel_imu.y() = m_device->GetGyroY();
	device->m_ang_vel_imu.z() = m_device->GetGyroZ();

	// rotate IMU velocity into vehicle body frame
	device->m_ang_vel_body = device->m_quat_body_nwu.rotateVector( device->m_ang_vel_imu );

	// update mavlink message with body-referenced angular velocity
	m_euler.roll_rate    = device->m_ang_vel_body.x();
	m_euler.pitch_rate   = device->m_ang_vel_body.y();
	m_euler.yaw_rate     = device->m_ang_vel_body.z();

	// Calculate roll pitch yaw attitude in degrees
	CalculateRollPitchYaw();

	// Calculate compass heading
	CalculateCompassHeading();

    UpdateFailures( false );
    return ERetcode::SUCCESS;
}

void IMU::CalculateRollPitchYaw()
{
    bno055::Vector<3> rpy;

    if( m_use055 )
    {
        rpy = GetRollPitchYaw( ((bno055::BNO055 *)m_device)->m_quatData055 );

        // y-x swap due to bno55 axis
        m_euler.roll   = RadiansToDegrees( rpy.y() );
        m_euler.pitch  = RadiansToDegrees( rpy.x() );
        m_euler.yaw    = RadiansToDegrees( rpy.z() );

    } else {
        rpy = GetRollPitchYaw( ((bno080::BNO080 *)m_device)->m_quat_body_nwu );

        m_euler.roll   = RadiansToDegrees( rpy.x() );
        m_euler.pitch  = -RadiansToDegrees( rpy.y() ); // flip sign for consistency
        m_euler.yaw    = RadiansToDegrees( rpy.z() );
        
        if (!m_euler.roll) {
            SendDebug(4000, 1, 1);
        }
    }
}

//en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
template<typename T>
bno055::Vector<3> IMU::GetRollPitchYaw( const T &quatIn )
{
    bno055::Vector<3> rpy;

    // Roll
    double a = 2 * ( quatIn.w() * quatIn.x() + quatIn.y() * quatIn.z() );
    double b = 1 - 2 * ( quatIn.x() * quatIn.x() + quatIn.y() * quatIn.y() );
    rpy.x()   = atan2( a, b );

    // Pitch
    double p = 2 * ( quatIn.w() * quatIn.y() - quatIn.z() * quatIn.x() );
    if ( abs( p ) >= 1 )
        rpy.y() = copysign( M_PI / 2, p ); // use 90 degrees if out of range
    else
        rpy.y() = asin( p );

    // Yaw - debug to map correct bno chip alignment of NWU
     double x = 2 * ((quatIn.x() * quatIn.y()) + (quatIn.w() * quatIn.z()));
     double y = pow(quatIn.w(),2) - pow(quatIn.z(),2) - pow(quatIn.y(),2) + pow(quatIn.x(),2);
     rpy.z() = atan2(y, x);

    return rpy;
}

void IMU::CalculateCompassHeading()
{
    double correction = 0.0;

    if( m_use055 )
    {
        correction = COMPASS_CORRECTION_BNO055;
    }else
    {
        correction = COMPASS_CORRECTION_BNO080;
    }

    double headingRads = QuaternionToYaw( m_quat.q3, m_quat.q4, m_quat.q1, m_quat.q2 );

    m_euler.heading = fmod( ( RadiansToDegrees( headingRads ) + correction ), COMPASS_ROSE );
}

double IMU::QuaternionToYaw( double w, double x, double y, double z )
{
    double a = 2 * ( (x * y) + (w * z) );
    double b = pow( w,2 ) - pow( z,2 ) - pow( y,2 ) + pow( x,2 );

    return atan2( b, a );
}

double IMU::RadiansToDegrees( double radians ){
    return ( radians * 180.0 / M_PI );
}

