#pragma once

// Includes
#include <ms5837_30ba.h>
#include "I2CMonitor.h"
//#include "Subsystem.h"

class DepthGauge : I2CDevice
{
public:
	enum class ESubstate : uint8_t
    {
        READING_CALIB_DATA = static_cast<uint8_t>( EReservedSubstate::COUNT ),
        CONVERTING_PRESSURE,
        CONVERTING_TEMPERATURE,
		PROCESSING_DATA
    };

	enum class ERetcode : uint8_t
	{
		SUCCESS = 0,
		ERROR_IO,
		ERROR_CRC_CHECK
	};

	// Methods
	explicit DepthGauge();

	void Initialize();
	void Post();
	void Update();
    void UpdateFailures( bool failureOccured );

private:
	bool m_readyToPublish = false;

	mavlink_orov_depth_reading_t 	m_data;
	mavlink_orov_depth_config_t 	m_config;

	uint8_t m_postAttempts = 0;

	uint16_t m_coefficients[8];
	uint32_t m_rawPressure;
	uint32_t m_rawTemperature;

	uint8_t m_address 						= ms5837_30ba::addresses::A;
	ms5837_30ba::EOversampleRate m_osr 		= ms5837_30ba::EOversampleRate::OSR_8192_SAMPLES;
	ms5837_30ba::TOversampleInfo m_osrInfo	= ms5837_30ba::constants::kOSRInfo[ (int)ms5837_30ba::EOversampleRate::OSR_8192_SAMPLES ];

	float m_waterMod		= 0.0f;

	float m_temperature_c;
	float m_pressure_mbar;

	// zeroing
	// Depth samples are being received at about 25 hz
	// 25 samples, ~1 seconds of data
	uint8_t m_maxDepthSamples = 25;
	uint8_t m_depthSampleCounter = 0;
	float m_depthZeroSum = 0;


	// Sensor methods
	ERetcode Reset();
	ERetcode ReadAndValidateCoefficients();
	ERetcode StartPressureConversion();
	ERetcode ReadRawResult( uint32_t &rawResult );
	ERetcode StartTemperatureConversion();
	ERetcode ProcessResults();

	// State methods
	bool PostAttempt();
	void HandleMessages();

	void Update_Active();
	void Update_Standby();
	void Update_Recovery();
	void Update_Disabled();

	void CalculateDepth();
};
