#include "DepthGauge.h"
#include "NState.h"

#include "SystemMonitor.h"
#include "CommLink.h"

namespace
{
	// Table of command modifiers and conversion times for each OSR
	constexpr float kWaterMods[ 3 ] =
	{
		0.01019716f,	// Fresh
		0.01007108f,	// Brackish
		0.00994500f		// Salt
	};
}


///////////////////////////////////////////////////////////////////

DepthGauge::DepthGauge()
    : I2CDevice( EI2CDeviceId::MS5837, 
                 OROV_TRIDENT_SUBSYSTEM_ID_DEPTH,
                 "DEPT", 25)
	, m_coefficients()
    , m_rawPressure(0)
    , m_rawTemperature(0)
    , m_temperature_c(0.0)
    , m_pressure_mbar(0.0)
{
}

void DepthGauge::Initialize()
{
	// Register with SystemMonitor
	SystemMonitor::RegisterSubsystem( &m_subsystem );

	// Register I2C device with monitor
    I2CMonitor::RegisterDevice( this );

	// Set initial configuration
	m_config = { 0, OROV_WATER_TYPE_FRESH, 0, 0, 0 };

	// Update state
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
	m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
}

void DepthGauge::Post()
{
	// Reset state and inform that the post result is being awaited
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING);
	m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

	// Check I2C availability
	if( m_i2c->GetState() != EI2CState::READY )
	{
		// Set error code and result
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
		return;
	}

	int postCount 	= 0;
	bool postResult = false;

	do
	{
		// Reset the WDT on each attempt
		SystemMonitor::ResetWDT();

		// Make an attempt to post
		postResult = PostAttempt();
		postCount++;
	}
	while( !postResult && postCount < 3 );

	if( postResult )
	{
		// Success
		m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
		m_subsystem.TransitionSubstate( ESubstate::CONVERTING_PRESSURE );
	}
	else
	{
		// Failure
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
	}
}

bool DepthGauge::PostAttempt()
{
	// Reset the device
	auto ret = Reset();
	if( ret != ERetcode::SUCCESS ){ return false; }

	// Wait 100ms
	xtimer_usleep( 100000 );

	// Read the sensor coefficients and perform the CRC check
	ret = ReadAndValidateCoefficients();
	if( ret != ERetcode::SUCCESS ){ return false; }

	return true;
}


/**
 * Avoid triggering resets for depth sensor, which may be missing for
 * ASV.  Wrap up the code here. 
 **/ 

void DepthGauge::UpdateFailures( bool failureOccured ) 
{
    
    if (failureOccured) {    
        // Commented to ignore failures
        //m_i2cDev.UpdateFailures( failureOccured );
    } else {
        I2CDevice::UpdateFailures( failureOccured );
    }
}


void DepthGauge::HandleMessages()
{
	int messageId = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessageID();
    
    switch( messageId )
    {
		case MAVLINK_MSG_ID_OROV_SUBSYSTEM_DATA_REQUEST:
        {
			mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();
            uint8_t targetMsgId = mavlink_msg_orov_subsystem_data_request_get_msg_id( message );

            if( targetMsgId == MAVLINK_MSG_ID_OROV_DEPTH_CONFIG )
            {
                // Signify spontaneous response
				m_config.sequence = -1;
				mavlink_msg_orov_depth_config_send_struct( CommLink::MAIN_CHANNEL, &m_config );
            }

			break;
		}

        case MAVLINK_MSG_ID_OROV_DEPTH_CONFIG:
        {
			mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();

			// Update sequence
			m_config.sequence = mavlink_msg_orov_depth_config_get_sequence( message );

			// Water type
			auto waterType = mavlink_msg_orov_depth_config_get_sequence( message );
			// Validate
			if( waterType < static_cast<uint8_t>( OROV_WATER_TYPE_COUNT ) )
			{
				m_config.water_type = waterType;
			}

			// User offset information
			m_config.user_offset_enabled = mavlink_msg_orov_depth_config_get_user_offset_enabled( message );
			m_config.zero_offset_user = mavlink_msg_orov_depth_config_get_zero_offset_user( message );

			// Send update
			mavlink_msg_orov_depth_config_send_struct( CommLink::MAIN_CHANNEL, &m_config );

			break;
		}

		default:
		{
			break;
		}
	}
}

void DepthGauge::Update()
{
	// Handle incoming commands
	HandleMessages();

	switch( m_subsystem.GetState() )
	{
		case OROV_SUBSYSTEM_STATE_ACTIVE:	{ Update_Active(); 		break; }
		case OROV_SUBSYSTEM_STATE_RECOVERY:	{ Update_Recovery();	break; }
		case OROV_SUBSYSTEM_STATE_STANDBY:	{ Update_Standby();		break; }
		case OROV_SUBSYSTEM_STATE_DISABLED:	{ break; }

		default: 
		{	// Move to the recovery state. These states aren't supported
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void DepthGauge::Update_Active()
{
	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( ESubstate::CONVERTING_PRESSURE ):
		{
			if( StartPressureConversion() != ERetcode::SUCCESS )
			{	// Failure, try another conversion in a bit
				m_subsystem.TransitionSubstateDelayed( ESubstate::CONVERTING_PRESSURE, m_osrInfo.conversionTime_us );
			}
			else
			{	// Sucess, move to active state and begin conversion
				m_subsystem.TransitionSubstateDelayed( ESubstate::CONVERTING_TEMPERATURE, m_osrInfo.conversionTime_us );
			}
			break;
		}

		case static_cast<uint8_t>( ESubstate::CONVERTING_TEMPERATURE ):
		{
			if( ReadRawResult( m_rawPressure ) != ERetcode::SUCCESS )
			{	// Failure, try another conversion in a bit
				m_subsystem.TransitionSubstateDelayed( ESubstate::CONVERTING_PRESSURE, m_osrInfo.conversionTime_us );
			}
			else
			{	
				if( StartTemperatureConversion() != ERetcode::SUCCESS )
				{	// Failure, try another conversion in a bit
					m_subsystem.TransitionSubstateDelayed( ESubstate::CONVERTING_PRESSURE, m_osrInfo.conversionTime_us );
				}
				else
				{	// Sucess
					m_subsystem.TransitionSubstateDelayed( ESubstate::PROCESSING_DATA, m_osrInfo.conversionTime_us );
				}
			}
			break;
		}

		case static_cast<uint8_t>( ESubstate::PROCESSING_DATA ):
		{
			if( ReadRawResult( m_rawTemperature ) != ERetcode::SUCCESS )
			{	// Failure, try another conversion in a bit
				m_subsystem.TransitionSubstateDelayed( ESubstate::CONVERTING_PRESSURE, m_osrInfo.conversionTime_us );
			}
			else
			{
				ms5837_30ba::ProcessRawValues( m_coefficients, m_rawPressure, m_rawTemperature, m_pressure_mbar, m_temperature_c );

				// Set timestamp
				m_data.timestamp = xtimer_now();

				// Calculate depth using compensated values and water type
				CalculateDepth();

				// Set pressure and temperature
				m_data.pressure 	= m_pressure_mbar;
				m_data.temperature 	= m_temperature_c;
				
				state::depth 		= m_data.depth;
				state::water_temp 	= m_data.temperature;

				// Send out updates
				if(m_readyToPublish)
				{
					mavlink_msg_orov_depth_reading_send_struct( CommLink::MAIN_CHANNEL, &m_data );
				}
				
				// Start a new conversion
				m_subsystem.TransitionSubstate( ESubstate::CONVERTING_PRESSURE );
			}
			break;
		}

		default:
		{
			// Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void DepthGauge::Update_Standby()
{
	// Check to see if I2C bus is back
	if( m_i2c->Available() )
	{
		// Restore the original state
		m_subsystem.RestoreStateAndSubstate();
	}
}

void DepthGauge::Update_Recovery()
{
	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( EReservedSubstate::RESET ):
		{
			// Attempt to reset
			auto ret = Reset();
			if( ret != ERetcode::SUCCESS )
			{
				// Failure, try again in 100ms
				m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 100000 );
			}
			else
			{
				// Sucess, read calib parameters after 10ms
				m_subsystem.TransitionSubstateDelayed( ESubstate::READING_CALIB_DATA, 10000 );
			}

			break;
		}

		case static_cast<uint8_t>( ESubstate::READING_CALIB_DATA ):
		{
			auto ret = ReadAndValidateCoefficients();
			if( ret != ERetcode::SUCCESS )
			{
				// Failure, try again in 100ms
				m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 100000 );
			}
			else
			{
				// Sucess, move to active state and begin conversion
				m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
				m_subsystem.TransitionSubstate( ESubstate::CONVERTING_PRESSURE );
			}

			break;
		}

		default:
		{
			// Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

DepthGauge::ERetcode DepthGauge::Reset()
{
	auto ret = ms5837_30ba::Reset( m_i2c, m_address );
	if( ret )
	{
		// Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}

DepthGauge::ERetcode DepthGauge::ReadAndValidateCoefficients()
{
	// Read the calibration coefficients
	if( ms5837_30ba::ReadCalCoefficients( m_i2c, m_address, m_coefficients ) )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	// Check the CRC
	if( !ms5837_30ba::CheckCRC4( m_coefficients ) )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_CRC_CHECK );
		UpdateFailures( true );
		return ERetcode::ERROR_CRC_CHECK;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}

DepthGauge::ERetcode DepthGauge::StartPressureConversion()
{
	// Start a pressure conversion
	auto ret = ms5837_30ba::StartPresConversion( m_i2c, m_address, m_osrInfo.commandMod );
	if( ret )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}


DepthGauge::ERetcode DepthGauge::ReadRawResult( uint32_t &rawResult )
{
	// Read the calibration coefficients
	if( ms5837_30ba::ReadRawConversion( m_i2c, m_address, rawResult ) )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}

DepthGauge::ERetcode DepthGauge::StartTemperatureConversion()
{
	// Read the calibration coefficients
	if( ms5837_30ba::StartTempConversion( m_i2c, m_address, m_osrInfo.commandMod ) )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}

void DepthGauge::CalculateDepth()
{
	// Calculate depth based on the water type
	m_data.depth = ( ( m_pressure_mbar - 1013.25f ) * kWaterMods[ m_config.water_type ] );

	// If we still need to gather more samples for the offset
	if(m_depthSampleCounter <= m_maxDepthSamples)
	{
		m_depthZeroSum = m_depthZeroSum + m_data.depth;
		++m_depthSampleCounter;
	}
	else if(!m_readyToPublish)
	{
		m_readyToPublish = true;
		m_config.zero_offset = m_depthZeroSum / m_maxDepthSamples;
	}
	
	// Apply offset
	if( m_config.user_offset_enabled )
	{
		m_data.depth = m_data.depth - m_config.zero_offset_user;
	}
	else
	{
		m_data.depth = m_data.depth - m_config.zero_offset;
	}
}
