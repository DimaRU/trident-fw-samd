#pragma once
#include <cstdint>
// Includes
namespace state
{
    extern bool isShallow;

    extern float depth;
    extern float water_temp;
    extern float internal_pressure;
    extern float internal_temp;
    extern float voltage;
    extern float current;
    extern float remainingPower;
    extern uint8_t discharging;
}