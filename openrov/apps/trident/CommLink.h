#pragma once

// Includes
#include <board.h>
#include <log.h>
#include <assert.h>

#include <MavlinkChannel.h>
#include "Subsystem.h"

class CommLink
{
public:
    // Constants
    static constexpr mavlink_channel_t MAIN_CHANNEL = MAVLINK_COMM_0;
    static constexpr mavlink_channel_t PORT_CHANNEL = MAVLINK_COMM_1;
    static constexpr mavlink_channel_t VERT_CHANNEL = MAVLINK_COMM_2;
    static constexpr mavlink_channel_t STAR_CHANNEL = MAVLINK_COMM_3;

    static MavlinkChannel* GetChannel( mavlink_channel_t channelIn );

    CommLink() 
        : m_subsystem{ OROV_TRIDENT_SUBSYSTEM_ID_COMM_LINK, "COMM" } 
    {
    }

    void Initialize();
    void Post();
    void Update();

private:
    Subsystem m_subsystem;

    bool PostCheck();
    void ReportLinkStats();

    // Mavlink channels
    static MavlinkChannel MavlinkChannels[ MAVLINK_COMM_NUM_BUFFERS ];

    mavlink_orov_trident_comm_link_stats_t m_stats;
    
    static constexpr int kMainBaudRate = 1500000;
    static constexpr int kMotorBaudRate = 115340;

    orutil::CTimer m_reportTimer;
};