#pragma once

#include "MCU.h"
#include "Subsystem.h"
#include <MavlinkChannel.h>

class Motor
{
public:
    Motor( 	MavlinkChannel* mavlinkChannelIn,
            OROV_TRIDENT_MCU_ID mcuIdIn,
            OROV_TRIDENT_SUBSYSTEM_ID subsysIdIn,
            const char* nameIn );

    void Initialize();
    void Update();

    void SendSpeedTarget( float targetIn );
    void SendMinMaxError( float min, float max, int32_t timeout );

private:
    // Constants
    static constexpr uint32_t kReliabilityPeriod_usec   = 10000;    // 10 ms
    static constexpr uint32_t kLivelinessPeriod_usec    = 3000000;  // 3 seconds
    static constexpr float kNeutralTarget_pct           = 0.0f;

    // Attributes
    MavlinkChannel* m_pChannel;
    MCU m_mcu;
    Subsystem m_subsystem;

    orutil::CTimer m_livelinessTimer;
    orutil::CTimer m_reliabilityTimer;

    float m_target = 0.0f;
    bool m_isMavVersionCompatible = false;

    const uint8_t k_id;

    // Mavlink data
    mavlink_orov_esc_feedback_t             m_feedback;
    mavlink_orov_esc_fault_alert_t          m_lastFault;
    mavlink_orov_esc_fault_warning_info_t   m_errorStats;
    mavlink_orov_esc_control_cmd_t          m_controlCommand;

    // Methods
    void HandleMessage_MCUStatus();
    void HandleComms();

    void ResetState();
};
