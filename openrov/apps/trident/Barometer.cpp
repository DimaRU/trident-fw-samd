#include "Barometer.h"
#include "NState.h"

#include "SystemMonitor.h"
#include "CommLink.h"


///////////////////////////////////////////////////////////////////

Barometer::Barometer()
    : I2CDevice( EI2CDeviceId::MPL3115,
                 OROV_TRIDENT_SUBSYSTEM_ID_BAROMETER,
                 "BARO", 20)

	, m_dataBuffer()
    , m_temperature_c(0.0)
    , m_pressure_mbar(0.0)
{
}

void Barometer::Initialize()
{
	// Register with SystemMonitor
	SystemMonitor::RegisterSubsystem( &m_subsystem );

	// Register I2C device with monitor
	I2CMonitor::RegisterDevice( this );

	m_statusTimer.Reset();

	// Update state
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
	m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
}

void Barometer::Post()
{
	// Reset state and inform that the post result is being awaited
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING);
	m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

	// Check I2C availability
	if( m_i2c->GetState() != EI2CState::READY )
	{
		// Set error code and result
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
		return;
	}

	int postCount 	= 0;
	bool postResult = false;

	do
	{
		// Reset the WDT on each attempt
		SystemMonitor::ResetWDT();

		// Make an attempt to post
		postResult = PostAttempt();
		postCount++;
	}
	while( !postResult && postCount < 3 );

	if( postResult )
	{   // Success
		m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
		m_subsystem.TransitionSubstateDelayed( ESubstate::STARTING_MEASUREMENT, m_sampleReadDelay_us );
	}
	else
	{   // Failure
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
	}
}

bool Barometer::PostAttempt()
{
	// Reset the device
	Reset();

	// Wait 500ms
	xtimer_usleep( mpl3115::constant::POR_DELAY_US );

    // Check the chip ID to verify chip is responsive
    auto ret = CheckChipID();
    if( ret != ERetcode::SUCCESS ){ return false; }

    // Configure device to be a barometer with our specified settings
    ret = ConfigureBarometer();
	if( ret != ERetcode::SUCCESS ){ return false; }

	return true;
}

void Barometer::Update()
{
	switch( m_subsystem.GetState() )
	{
		case OROV_SUBSYSTEM_STATE_ACTIVE:	{ Update_Active(); 		break; }
		case OROV_SUBSYSTEM_STATE_RECOVERY:	{ Update_Recovery();	break; }
		case OROV_SUBSYSTEM_STATE_STANDBY:	{ Update_Standby();		break; }
		case OROV_SUBSYSTEM_STATE_DISABLED:	{ break; }

		default: 
		{	// Move to the recovery state. These states aren't supported
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void Barometer::Update_Active()
{

	if( m_statusTimer.HasElapsed( m_kStatusTimerPeriod_us ) )
	{
		// Check Health
		if( CheckHealth() != ERetcode::SUCCESS )
		{
			// Go to recovery mode and restart the sensor
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
		}
	}

	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

        case static_cast<uint8_t>( ESubstate::WAITING_FOR_DATA ):
		{
            if( CheckDataReady() != ERetcode::SUCCESS )
			{	// Wait for the specified period until data is available
				m_subsystem.TransitionSubstateDelayed( ESubstate::WAITING_FOR_DATA, mpl3115::constant::DATA_READY_CHECK_DELAY_US );
			}
			else
			{	// Data available, perform read
                if( ReadMeasurement() != ERetcode::SUCCESS )
                {	// Failure, do nothing
                }
                else
                {
                    // Set timestamp
                    m_data.timestamp = xtimer_now();

                    // Set pressure and temperature
                    m_data.pressure 	= m_pressure_mbar;
					m_data.temperature 	= m_temperature_c;
					
					state::internal_pressure 	= m_data.pressure;
					state::internal_temp 	    = m_data.temperature;

                    // Send out updates
                    mavlink_msg_orov_barometer_reading_send_struct( CommLink::MAIN_CHANNEL, &m_data );
                }

                // Start a new measurement
                m_subsystem.TransitionSubstate( ESubstate::STARTING_MEASUREMENT );
			}

			break;
		}

		case static_cast<uint8_t>( ESubstate::STARTING_MEASUREMENT ):
		{
			if( StartMeasurement() != ERetcode::SUCCESS )
			{	// Failure, try another conversion in a bit
				m_subsystem.TransitionSubstateDelayed( ESubstate::STARTING_MEASUREMENT, m_sampleReadDelay_us );
			}
			else
			{	// Sucess, wait for data to be available
				m_subsystem.TransitionSubstateDelayed( ESubstate::WAITING_FOR_DATA, m_sampleReadDelay_us );
			}
			break;
		}

		default:
		{   // Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void Barometer::Update_Standby()
{
	// Check to see if I2C bus is back
	if( m_i2c->Available() )
	{
		// Restore the original state
		m_subsystem.RestoreStateAndSubstate();
	}
}

void Barometer::Update_Recovery()
{
	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( EReservedSubstate::RESET ):
		{
			// Reset and reconfigure in the specified period
			Reset();
            m_subsystem.TransitionSubstateDelayed( ESubstate::REINITIALIZING_DEVICE, mpl3115::constant::POR_DELAY_US );
			break;
		}

        case static_cast<uint8_t>( ESubstate::REINITIALIZING_DEVICE ):
		{
            // Check the chip ID to verify chip is responsive
            auto ret = CheckChipID();
            if( ret != ERetcode::SUCCESS )
            {   // Failure, try again in 100ms
				m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 3000000 );
                break;
            }

            // Configure device to be a barometer with our specified settings
            ret = ConfigureBarometer();
            if( ret != ERetcode::SUCCESS )
            {   // Failure, try again in 100ms
				m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 3000000 );
                break;
            }

            // Start a measurement
            m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
            m_subsystem.TransitionSubstateDelayed( ESubstate::STARTING_MEASUREMENT, m_sampleReadDelay_us );

			break;
		}

		default:
		{
			// Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

Barometer::ERetcode Barometer::Reset()
{
    // Ignore the results of the i2c transaction because this device always NACKs on soft reset
	mpl3115::Reset( m_i2c );
	return ERetcode::SUCCESS;
}

Barometer::ERetcode Barometer::CheckChipID()
{
    uint8_t chipId = 0;
    auto ret = mpl3115::ReadChipId( m_i2c, chipId );
	if( ret )
	{
		// Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

    // Compare against proper ID
    if( chipId != mpl3115::constant::CHIP_ID )
    {
        UpdateFailures( true );
        m_subsystem.SetLastErrorCode( ERetcode::ERROR_BAD_CHIP_ID );
        return ERetcode::ERROR_BAD_CHIP_ID;
    }

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}

Barometer::ERetcode Barometer::ConfigureBarometer()
{
    auto ret = mpl3115::ConfigureBarometer( m_i2c, m_osr );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

    return ERetcode::SUCCESS;
}

Barometer::ERetcode Barometer::StartMeasurement()
{
    auto ret = mpl3115::StartMeasurement( m_i2c );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

    return ERetcode::SUCCESS;
}

Barometer::ERetcode Barometer::CheckDataReady()
{
    bool dataReady = false;
    auto ret = mpl3115::CheckDataReady( m_i2c, dataReady );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

    if( !dataReady )
    {
        return ERetcode::NO_DATA_AVAILABLE;
    }

    return ERetcode::SUCCESS;
}

Barometer::ERetcode Barometer::ReadMeasurement()
{
    auto ret = mpl3115::ReadPressureAndTemp( m_i2c, m_pressure_mbar, m_temperature_c );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
    }

    return ERetcode::SUCCESS;
}

Barometer::ERetcode Barometer::CheckHealth()
{
	uint8_t reg;
	auto ret = mpl3115::ReadControlRegister1( m_i2c, reg );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
    }

	// If the SBYB bit is not 1, the chip is waiting to be activated
	if( !( reg & 0x01 ) )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_BAD_STATE );
		UpdateFailures( false );
		return ERetcode::ERROR_BAD_STATE;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}
