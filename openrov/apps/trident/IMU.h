#pragma once
/*
 *  Notes:
 *  	Examples taken from SparkFun Arduino Example
 *  		https://github.com/sparkfun/SparkFun_BNO080_Arduino_Library
 * 
 *  Combined 055/080/085 driver by Peter Naulls <peter.naulls@sofarocean.com>
 */
#include <bno055.h>
#include <bno080.h>

#include "I2CMonitor.h"
#include "Subsystem.h"

class IMU /*: I2CDevice*/
{
public:
	enum class ESubstate : uint8_t
	    {
	        POST_RESET = (uint8_t)EReservedSubstate::COUNT,
			CONFIGURING_CHIP,
			WAITING_FOR_ACTIVE,
			ACTIVE
	    };

		enum class ERetcode : uint8_t
		{
			SUCCESS = 0,
			ERROR_IO,
			ERROR_FAILED_POST,
			ERROR_INVALID_FIRMWARE,
			ERROR_INVALID_MODE,
			ERROR_INVALID_ID,
			ERROR_INCORRECT_STATE,
			ERROR_FAILED_HEALTH_CHECK,
			ERROR_GENERIC
		};

		// Methods
		explicit IMU();        
        IMU(const IMU&) = delete;

		void Initialize();
		void Post();
		void Update();
        
        static void                             SendDebug(uint32_t time, uint8_t ind, float value);

private:
		orutil::CTimer 							m_statusTimer;
		orutil::CTimer 							m_dataTimer;
        orutil::CTimer 							m_resetTimer;

        I2CDevice                               *m_i2cDevice055;
        I2CDevice                               *m_i2cDevice080;
        I2CDevice                               *m_i2cDevice;
        
        bool                                    m_use055;
        bool                                    m_firstReset = true;
                
        // Monitor variables -- needed?
		uint8_t 								m_postAttempts;
		uint8_t 								m_opmodeWaitCounter;
		const uint32_t 							m_kStatusTimerPeriod_us = 1000000;
                        
		// Mavlink data
		mavlink_orov_imu_attitude_quaternion_t 	m_quat;
		mavlink_orov_imu_attitude_euler_t		m_euler;

		mavlink_orov_imu_calibration_t 			m_calib;
		mavlink_orov_imu_mode_t 				m_mode;

        // Device references
        BNO                                     *m_device055;
        BNO                                     *m_device080;
        BNO                                     *m_device;        
		
		constexpr static double COMPASS_ROSE 				= 360.0;
		constexpr static double COMPASS_CORRECTION_BNO080 	= 360.0;
		constexpr static double COMPASS_CORRECTION_BNO055 	= 280.0;
		constexpr static double M_PI                        = 3.14159;
        
        void                                    SetDevice(bool use055);
        void                                    UpdateFailures( bool failureOccured );
        void                                    DetectDevice();        
        
		// Sensor methods
		ERetcode 								Reset();					// Soft reset of the chip
		ERetcode 								VerifyPostReset();			// Verifies chip is in a good state after reset

		ERetcode 								Configure();				// Sets our settings for this application
        ERetcode                                CheckStatus( const bno055::ESystemStatus &statusIn );
		ERetcode 								ReadAttitudeData055();		// Reads the quaternion data from the chip
		ERetcode 								ReadAttitudeData080();		// Reads the quaternion data from the chip
        ERetcode                                ReadCalibration();			// Reads the calibration data from the chip
        ERetcode                                CheckHealth();				// Routine check to make sure chip is still in the proper state

        void                                    CalculateRollPitchYaw();
        void                                    CalculateCompassHeading();

        double                                  QuaternionToYaw( double w, double x, double y, double z );
        double                                  RadiansToDegrees( double radians );

        template<typename T>
        bno055::Vector<3>                       GetRollPitchYaw( const T &quatIn );

		// State methods
		bool 									PostAttempt();

		void 									Update_Active();
		void 									Update_Standby();
		void 									Update_Recovery();
		void 									Update_Disabled();
};
