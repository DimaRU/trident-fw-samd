#pragma once

// Includes
#include <orutil.h>
#include <periph/pwm.h>

#include "MavlinkBridgeHeader.h"
#include "Subsystem.h"

class ForwardLights
{
public:
    ForwardLights( pwm_t pwmDevIn, uint8_t pwmChannelIn, uint32_t pwmFrequencyIn, uint16_t pwmResolutionIn );

    void Initialize();
    void Post();
    void Update();

private:
    Subsystem m_subsystem;

    mavlink_orov_forward_light_state_t m_state;

    enum ESubstate : uint8_t
    {
        FORWARD_LIGHTS_SUBSTATE_IDLE = 0,
        FORWARD_LIGHTS_SUBSTATE_RAMPING,
        FORWARD_LIGHTS_SUBSTATE_GREETING
    };

    pwm_t           m_pwmDev;
    uint8_t         m_pwmChannel;
    uint32_t        m_pwmFrequency;
    uint16_t        m_pwmResolution;

    // Percent representation of power (linear)
    float           m_targetPower       = 0.0f;     
    float           m_currentPower      = 0.0f;

    // PWM duty cycle representation of power (non-linear)
    uint16_t        m_lastPower_pwm     = 0;       
    uint16_t        m_targetPower_pwm   = 0;
    uint16_t        m_currentPower_pwm  = 0;

    orutil::CTimer  m_controlTimer;

    // Variables used to perform greeting
	int m_greetState 		    = 0;    // LOW/HIGH
	int m_greetCycle 			= 0;
	const int m_cycleCount 	    = 6;
    const int m_greetDelay_us   = 300000;

    // Methods
    void HandleMessages();
    void ActiveState();

    uint16_t PercentToDutyCycle( float percentIn );
};

