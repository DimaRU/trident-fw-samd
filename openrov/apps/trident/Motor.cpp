#include "Motor.h"
#include "CommLink.h"

Motor::Motor( 	MavlinkChannel* mavlinkChannelIn,
				OROV_TRIDENT_MCU_ID mcuIdIn,
				OROV_TRIDENT_SUBSYSTEM_ID subsysIdIn,
				const char* nameIn )
	: m_pChannel{ mavlinkChannelIn }
	, m_mcu{ mcuIdIn }
	, m_subsystem{ subsysIdIn, nameIn }
	, k_id{ m_mcu.GetId() }
{
}

void Motor::Initialize()
{
	ResetState();

	// Reset timers
	m_livelinessTimer.Reset();
	m_reliabilityTimer.Reset();
}

void Motor::ResetState()
{
	// Set initial state to unknown. Listen to ESC for actual state
    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_UNKNOWN );
	m_subsystem.TransitionSubstate( EReservedSubstate::RESET );

	m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );
}

void Motor::HandleMessage_MCUStatus()
{
	int messageId = m_pChannel->GetCurrentMessageID();
	if( messageId == MAVLINK_MSG_ID_OROV_MCU_STATUS )
	{
		mavlink_message_t* message = m_pChannel->GetCurrentMessage();
		m_mcu.UpdateStatus( message );

		// Check the mavlink version for compatibility
		m_isMavVersionCompatible = ( m_mcu.GetMavlinkVersion() == MAVLINK_VERSION );

		// Forward on status message
		m_mcu.SendStatus( CommLink::MAIN_CHANNEL );
	}
}

void Motor::HandleComms()
{
	// Only handle ESC messages if they have the same mavlink version to the MCU's version
	if( m_isMavVersionCompatible )
	{
		int messageId 			= m_pChannel->GetCurrentMessageID();
		mavlink_message_t *msg 	= m_pChannel->GetCurrentMessage();

		switch( messageId )
		{
			case MAVLINK_MSG_ID_OROV_SUBSYSTEM_STATUS:
			{
				m_subsystem.UpdateStatus( m_pChannel->GetCurrentMessage() );
				m_subsystem.SendStatus( CommLink::MAIN_CHANNEL );
				break;
			}

			case MAVLINK_MSG_ID_OROV_ESC_FEEDBACK:
			{
				mavlink_msg_orov_esc_feedback_decode( msg, &m_feedback );
				m_feedback.id = m_mcu.GetId();
				mavlink_msg_orov_esc_feedback_send_struct( CommLink::MAIN_CHANNEL, &m_feedback );

				break;
			}

			case MAVLINK_MSG_ID_OROV_ESC_FAULT_ALERT:
			{
				mavlink_msg_orov_esc_fault_alert_decode( msg, &m_lastFault );
				m_lastFault.id = m_mcu.GetId();
				mavlink_msg_orov_esc_fault_alert_send_struct( CommLink::MAIN_CHANNEL, &m_lastFault );
				break;
			}

			case MAVLINK_MSG_ID_OROV_ESC_FAULT_WARNING_INFO:
			{
				mavlink_msg_orov_esc_fault_warning_info_decode( msg, &m_errorStats );
				m_errorStats.id = m_mcu.GetId();
				mavlink_msg_orov_esc_fault_warning_info_send_struct( CommLink::MAIN_CHANNEL, &m_errorStats );
				break;
			}

			default:
			{
				break;
			}
		}
	}
}

void Motor::Update()
{
	// Receive and parse message
	m_pChannel->Update();

	// Handle MCUStatus messages
	HandleMessage_MCUStatus();

	// // Handle any other messages from the ESC
	HandleComms();
}


/**
 * -1.0 to 1.0 motor target speed
 */ 
void Motor::SendSpeedTarget( float targetIn )
{
	mavlink_msg_orov_esc_control_cmd_send( m_pChannel->m_channel, k_id, OROV_ESC_CMD_ID_SPEED_PERCENT, targetIn );
}


/**
 * Targets and timeout for min/max error handling 
 * 
 * Timeout value:
 *      -2  - Firmware default handling
 *      -1  - Throttle must return to zero first. 
 *       0  - Immediately recover
 *       >0 - Wait time in milliseconds
 */ 

void Motor::SendMinMaxError( float min, float max, int32_t timeout )
{
    mavlink_msg_orov_trident_motor_config_command_send( m_pChannel->m_channel, k_id, min, max, timeout);    
}








