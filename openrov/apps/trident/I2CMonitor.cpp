// Includes
#include "I2CMonitor.h"
#include "CommLink.h"
#include "SystemMonitor.h"

#include <periph/gpio.h>
#include <xtimer.h>

// Static members
I2C I2CMonitor::I2CMain( I2C_0, i2c_speed_t::I2C_SPEED_FAST );
I2CDevice* I2CMonitor::m_devices[ static_cast<uint8_t>( EI2CDeviceId::COUNT ) ];
int I2CMonitor::m_registeredDevices = 0;
bool I2CMonitor::doHardReset = false;
bool I2CMonitor::re_init = false;
///////////////
void I2CMonitor::Initialize()
{
    LOG_DEBUG( "I2CMonitor::Initialize\n" );

    // Set status pointer
    SystemMonitor::RegisterSubsystem( &m_subsystem );

    // Perform a hard reset
    if( !re_init )
    {mavlink_msg_debug_send(CommLink::MAIN_CHANNEL, 333, 0, 0);
        HardReset();
    }

    m_monitorTimer.Reset();
    m_resetTimer.Reset();
    m_reportTimer.Reset();

    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
}

void I2CMonitor::Post()
{
    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING );
    m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

    // First check the state
    EI2CState state = I2CMain.GetState();
    int postCount = 0;
    int postLimit = 3;

    while( (state != EI2CState::READY ) && ( postCount < postLimit ) )
    {
        // Perform a full reinitialization
        LOG_DEBUG( "I2CMonitor::Post attempt\n" );

        // Reset the WDT for safety
        SystemMonitor::ResetWDT();

        // Perform a hard reset
        HardReset();
        ++postCount;
    }

    if( state == EI2CState::READY )
    {
        LOG_DEBUG( "I2CMonitor::Post: Success!\n" );
        m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
        m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );
    }
    else
    {
        LOG_DEBUG( "I2CMonitor::Post: Failure!\n" );
        m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
        m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );
    }
}

void I2CMonitor::Update()
{
    if( m_reportTimer.HasElapsed( 5000000 ) )
    {
        ReportStats();
    }

    switch( m_subsystem.GetState() )
    {
        case OROV_SUBSYSTEM_STATE_ACTIVE:
        {
            // Peform monitoring
            if( m_monitorTimer.HasElapsed( 1000000 ) )
            {
                // First check device failures
                if( DeviceFailureLimitsReached() )
                {
                    LOG_INFO( "I2CMonitor: Failure limits reached!\n" );
                    m_stats.limit_break_count++;

                    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
                    break;
                }

                // Next check the SAMD21's bus state
                if( BusFailureOccurred() )
                {
                    LOG_INFO( "I2CMonitor: Bus failure!\n" );
                    m_stats.bus_failure_count++;

                    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
                    break;
                }

                // Make sure peripheral is enabled in general
                if( !I2CMain.Available() )
                {
                    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
                    break;
                }
            }

            // No problems
            break;
        }

        case OROV_SUBSYSTEM_STATE_RECOVERY:
        {
            if( m_resetTimer.HasElapsed( 100000 ) )
            {
                m_stats.recovery_count++;

                // Peform recovery tasks
                // The reset mechanisms internally send any event information necessary
                if( m_shouldPerformHardReset )
                {
                    HardReset();

                    if( I2CMain.Available() )
                    {
                        // Reset was a success. Use a soft reset on the next failure.
                        m_shouldPerformHardReset = false;

                        m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
                    }
                }
                else
                {
                    SoftReset();

                    // Check results
                    if( I2CMain.Available() )
                    {
                        m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
                    }
                    else
                    {
                        // Reset failed. Elevate to a hard reset.
                        m_shouldPerformHardReset = true;
                    }
                }
            }

            break;
        }

        default:
        {
            // Should not reach here
            m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
            break;
        }
    }
}

bool I2CMonitor::DeviceFailureLimitsReached()
{
    // Loop through each device and check to see if all devices have reached their max consecutive error limit
    int limitBreaks = 0;
    for( uint8_t i = 0; i < static_cast<uint8_t>( EI2CDeviceId::COUNT ); ++i )
    {
        if( m_devices[ i ] != nullptr )
        {
            if( m_devices[ i ]->FailureLimitReached() )
            {
                ++limitBreaks;
            }
        }
    }

    return ( ( m_registeredDevices == 0 ) ? false : limitBreaks == m_registeredDevices );
}

bool I2CMonitor::BusFailureOccurred()
{
    // Read the bus status
    auto ret = I2CMain.CheckBusHealth();
    if( ret )
    {
        return true;
    }

    return false;
}

// Hard Reset performs a power cycle for devices on the bus before performing a soft reset.
// The intent is that this will rescue us from any slaves that are holding down the line.
void I2CMonitor::HardReset()
{
    LOG_DEBUG( "I2CMonitor::HardReset()\n" );

    m_stats.hard_reset_count++;

    // Alert devices that a bus reset is occurring
    SendEvent( EI2CEvent::POWER_RESET );

    // Cycle the power for all of the devices on the I2C bus (except the fuelgauge)
    CyclePowerI2CMain();

    // Perform a soft reset. This will make an attempt to re-enable the interface.
    SoftReset();
}

// For soft resets, we first clock a bunch of SCL pulses to free any pending/stuck transactions.
// After that, we reset the peripheral internal to the MCU. This should fix most I2C engine failures caused
// by timing being out of timing tolerance, noise, etc.
void I2CMonitor::SoftReset()
{
    LOG_DEBUG( "I2CMonitor::SoftReset()\n" );

    m_stats.soft_reset_count++;

    // Alert devices that the bus is unavailable
    SendEvent( EI2CEvent::BUS_UNAVAILABLE );

    // Check current health and perform a toggle recovery if necessary
    auto ret = I2CMain.PerformToggleRecovery();
    if( ret )
    {   // Failure
        if( (ret == I2C_RETCODE_ERROR_SCL_LATCHED) || (ret == I2C_RETCODE_ERROR_SDA_LATCHED) || (ret == I2C_RETCODE_ERROR_SDASCL_LATCHED) )
        {
            m_stats.latch_failure_count++;
        }

        mavlink_msg_debug_send(CommLink::MAIN_CHANNEL, 333, 0, 0);
        m_subsystem.SetLastErrorCode( ret );
        return;
    }

    // Enable the interface. Part of the enable process is to reset the peripheral.
    ret = I2CMain.Enable();
    if( ret )
    {   // Failure
        m_subsystem.SetLastErrorCode( ret );
    }
    else
    {
        SendEvent( EI2CEvent::BUS_AVAILABLE );
    }
}

void I2CMonitor::ReportStats()
{
    mavlink_msg_orov_i2c_monitor_stats_send_struct( CommLink::MAIN_CHANNEL, &m_stats );

    LOG_INFO( "I2CStats: REC:%" PRIu32 ", HARD:%" PRIu32 ", SOFT:%" PRIu32 ", LIM:%" PRIu32 ", BUS:%" PRIu32 ", LATCH:%" PRIu32 "\n",
        m_stats.recovery_count,
        m_stats.hard_reset_count,
        m_stats.soft_reset_count,
        m_stats.limit_break_count,
        m_stats.bus_failure_count,
        m_stats.latch_failure_count );
}

void I2CMonitor::CyclePowerI2CMain()
{
    LOG_DEBUG( "I2CMonitor::CyclePowerI2CMain()\n" );

    // Cycle power with 10ms between
    gpio_clear( I2C0_PWR_PIN );
    xtimer_usleep( 10000 );
    gpio_set( I2C0_PWR_PIN );
    xtimer_usleep( 10000 );
}

void I2CMonitor::SendEvent( EI2CEvent eventIn )
{
    LOG_DEBUG( "I2CMonitor::SendEvent(%s)\n", I2CDevice::EventToString( eventIn ) );

    for( uint8_t i = 0; i < static_cast<uint8_t>( EI2CDeviceId::COUNT ); ++i )
    {
        if( m_devices[ i ] != nullptr )
        {
            m_devices[ i ]->Callback( eventIn );
        }
    }
}
