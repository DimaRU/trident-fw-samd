#include "FuelGauge.h"
#include "NState.h"

#include "SystemMonitor.h"
#include "CommLink.h"

#include <bq34z100g1.h>

///////////////////////////////////////////////////////////////////

FuelGauge::FuelGauge( )
    : I2CDevice( EI2CDeviceId::BQ34Z100G1, 
                 OROV_TRIDENT_SUBSYSTEM_ID_FUEL_GAUGE,
                 "FUEL", 20)
	, m_voltage_mv(0)
	, m_avgCurrent_ma(0)
	, m_insCurrent_ma(0)
	, m_avgPower_mw(0)
	, m_stateOfCharge_pct(0)
	, m_remainingCapacity_mah(0)
	, m_batteryTemperature_k(0)
	, m_flags(0)
	, m_fullCapacity_mah(0)
	, m_averageTimeToEmpty_mins(0)
	, m_cycleCount(0)
	, m_stateOfHealth_pct(0)
{
}

void FuelGauge::Initialize()
{
	// Register with SystemMonitor
	SystemMonitor::RegisterSubsystem( &m_subsystem );

	// Register I2C device with monitor
	I2CMonitor::RegisterDevice( this );

	// Update state
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
	m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
}

void FuelGauge::Post()
{
	// Reset state and inform that the post result is being awaited
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING);
	m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

	// Check I2C availability
	if( m_i2c->GetState() != EI2CState::READY )
	{
		// Set error code and result
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
		return;
	}

	int postCount 	= 0;
	bool postResult = false;

	do
	{
		// Reset the WDT on each attempt
		SystemMonitor::ResetWDT();

		// Make an attempt to post
		postResult = PostAttempt();
		postCount++;
	}
	while( !postResult && postCount < 3 );

	if( postResult )
	{   // Success
		m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
		m_subsystem.TransitionSubstate( ESubstate::ACTIVE );
	}
	else
	{   // Failure
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
	}
}

bool FuelGauge::PostAttempt()
{
	LOG_DEBUG( "FUELGAUGE POST\n" );
	// Check that the chip is responsive and in the correct state
	auto ret = CheckHealth();
	if( ret != ERetcode::SUCCESS ){ return false; }

	return true;
}

void FuelGauge::Update()
{
	switch( m_subsystem.GetState() )
	{
		case OROV_SUBSYSTEM_STATE_ACTIVE:	{ Update_Active(); 		break; }
		case OROV_SUBSYSTEM_STATE_RECOVERY:	{ Update_Recovery();	break; }
		case OROV_SUBSYSTEM_STATE_STANDBY:	{ Update_Standby();		break; }
		case OROV_SUBSYSTEM_STATE_DISABLED:	{ break; }

		default: 
		{	// Move to the recovery state. These states aren't supported
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void FuelGauge::Update_Active()
{
	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( ESubstate::ACTIVE ):
		{
			// Read battery status info (voltage, current, etc)
			ReadStatus();

			// Check battery health info and connectivity every 10 secs
			if( m_healthTimer.HasElapsed( 10000000 ) )
			{
				if( CheckHealth() == ERetcode::ERROR_BAD_STATUS )
				{
					// Disable the sensor. It is not configured correctly
					m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_DISABLED );
					m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
				}

				// Read battery health info
				ReadHealth();
			}

			// Update again in a second
			m_subsystem.TransitionSubstateDelayed( ESubstate::ACTIVE, 1000000 );

			break;
		}

		default:
		{   // Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void FuelGauge::Update_Standby()
{
	// Check to see if I2C bus is back
	if( m_i2c->Available() )
	{
		// Restore the original state
		m_subsystem.RestoreStateAndSubstate();
	}
}

void FuelGauge::Update_Recovery()
{
	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( EReservedSubstate::RESET ):
		{
			// Check that the chip is responsive with a simple command
			ERetcode ret = CheckHealth();
    		if( ret == ERetcode::ERROR_IO )
			{
				// Try again in a bit
				m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 1000000 );
			}
			else if( ret == ERetcode::ERROR_BAD_STATUS )
			{
				// This battery gauge was not sealed. Do not use it.
				m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_DISABLED );
				m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			}
			else
			{
				m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
            	m_subsystem.TransitionSubstate( ESubstate::ACTIVE );
			}

			break;
		}

		default:
		{
			// Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}


FuelGauge::ERetcode FuelGauge::ReadStatus()
{
	i2c_retcode_t ret;
	ret = bq34z100g1::ReadVoltage( m_i2c, m_voltage_mv );						if( ret ){  goto fail; }
	ret = bq34z100g1::ReadAverageCurrent( m_i2c, m_avgCurrent_ma );			if( ret ){  goto fail; }
	ret = bq34z100g1::ReadInstantCurrent( m_i2c, m_insCurrent_ma );			if( ret ){  goto fail; }
	ret = bq34z100g1::ReadAveragePower( m_i2c, m_avgPower_mw );				if( ret ){  goto fail; }
	ret = bq34z100g1::ReadStateOfCharge( m_i2c, m_stateOfCharge_pct );			if( ret ){  goto fail; }
	ret = bq34z100g1::ReadRemainingCapacity( m_i2c, m_remainingCapacity_mah );	if( ret ){  goto fail; }
	ret = bq34z100g1::ReadTemperature( m_i2c, m_batteryTemperature_k );		if( ret ){  goto fail; }
	ret = bq34z100g1::ReadFlags( m_i2c, m_flags );								if( ret ){  goto fail; }

	ReportStatus();

	// Success
	UpdateFailures( false );
	return ERetcode::SUCCESS;

fail:
	// Failure
	m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO ); 
	UpdateFailures( true ); 
	return ERetcode::ERROR_IO;
}


FuelGauge::ERetcode FuelGauge::ReadHealth()
{
	i2c_retcode_t ret;
	ret = bq34z100g1::ReadFullChargeCapacity( m_i2c, m_fullCapacity_mah );			if( ret ){  goto fail; }
	ret = bq34z100g1::ReadAverageTimeToEmpty( m_i2c, m_averageTimeToEmpty_mins );	if( ret ){  goto fail; }
	ret = bq34z100g1::ReadCycleCount( m_i2c, m_cycleCount );						if( ret ){  goto fail; }
	ret = bq34z100g1::ReadStateOfHealth( m_i2c, m_stateOfHealth_pct );				if( ret ){  goto fail; }

	ReportHealth();

	// Success
	UpdateFailures( false );
	return ERetcode::SUCCESS;

fail:
	// Failure
	m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO ); 
	UpdateFailures( true ); 
	return ERetcode::ERROR_IO;
}


FuelGauge::ERetcode FuelGauge::CheckHealth()
{
	// If the chip isn't sealed and IT is not enabled, it has likely not been flashed or is improperly setup

	bool val;
	auto ret = bq34z100g1::IsSealed( m_i2c, val );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
    }
	if( !val )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_BAD_STATUS );
		UpdateFailures( false );
		return ERetcode::ERROR_BAD_STATUS;
	}

	ret = bq34z100g1::IsITEnabled( m_i2c, val );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
    }
	if( !val )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_BAD_STATUS );
		UpdateFailures( false );
		return ERetcode::ERROR_BAD_STATUS;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}

void FuelGauge::ReportStatus()
{
	m_status.timestamp 		= xtimer_now();
	m_status.voltage 		= m_voltage_mv;
	m_status.avg_current 	= m_avgCurrent_ma;
	m_status.current 		= m_insCurrent_ma;
	m_status.avg_power 		= m_avgPower_mw;
	m_status.remaining_pct 	= m_stateOfCharge_pct;
	m_status.remaining_mah 	= m_remainingCapacity_mah;
	m_status.batt_temp 		= static_cast<float>( m_batteryTemperature_k ) - 273.15f;
	m_status.flags 			= m_flags;

	state::voltage           = static_cast<float>( m_voltage_mv ) / 1000.0f;
	state::current           = static_cast<float>( m_insCurrent_ma ) / 1000.0f;
	state::remainingPower    = static_cast<float>( m_stateOfCharge_pct ) / 100.0f;
	state::discharging			 = static_cast<uint8_t>( m_flags & 1);

	// Send out updates
	mavlink_msg_orov_fuelgauge_status_send_struct( CommLink::MAIN_CHANNEL, &m_status );   
}

void FuelGauge::ReportHealth()
{
	m_health.timestamp 				= xtimer_now();
	m_health.full_charge_capacity 	= m_fullCapacity_mah;
	m_health.avg_time_to_empty 		= m_averageTimeToEmpty_mins;
	m_health.cycle_count 			= m_cycleCount;
	m_health.state_of_health 		= static_cast<float>( m_stateOfHealth_pct ) / 100.0f;

	// Send out updates
	mavlink_msg_orov_fuelgauge_health_send_struct( CommLink::MAIN_CHANNEL, &m_health );  
}
