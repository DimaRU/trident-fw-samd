#include "BatteryFlasher.h"

#include "SystemMonitor.h"
#include "CommLink.h"

#include <string.h>

// Work around to build on arm-none-eabi-gcc (6.3.1) on Ubuntu Bionic
// Apparently this isn't a version we want to use, so this is only for 
// testing the build. 

#if __GNUC__ > 5

namespace
{
   char *strtok_r(char *str, const char *delim, char **save)
   {
       char *res, *last;

       if( !save )
           return strtok(str, delim);
       if( !str && !(str = *save) )
           return NULL;
       last = str + strlen(str);
       if( (*save = res = strtok(str, delim)) )
       {
           *save += strlen(res);
           if( *save < last )
               (*save)++;
           else
               *save = NULL;
       }
       return res;
   }
}

#endif


namespace
{
    // Helper to convert ms to us for waits
    template <typename T>
    T ConvertMillisecondsToMicroSeconds(T millisecondsIn)
    {
        return millisecondsIn*1000;
    }

    // To surpress unused var warning
    template<class T> void ignore( const T& ) { }

    // TI Flash stream wait 
    struct TWait
    {
        orutil::CTimer timer;
        uint32_t waitTime_us;
    };
    TWait flashWait;

    // Timers
    orutil::CTimer statusUpdateTimer;
    constexpr uint32_t statusUpdateRate = 1000000;
    
    // State of parsing
    struct TParsingState
    {
        // The byte index we are on for the firmware
        size_t firmwareIndex;

        // The data from the parsed firmware file
        uint8_t parsingDataBuffer[bq34z100_interface::MAX_DATA_BYTES_PER_LINE];
        uint16_t parsingDataBufferLength; 

        // The payload for the i2c
        uint8_t payloadBuffer[bq34z100_interface::MAX_DATA_BYTES_PER_LINE];
        uint16_t payloadBufferLength;

        // the data string
        char dataString[bq34z100_interface::MAX_DATA_BYTES_PER_LINE];
        uint16_t dataStringLength;

        // Data to compare for compares
        uint8_t dataToCompare[bq34z100_interface::MAX_DATA_BYTES_PER_LINE];
        uint16_t failedCompareCount;

        void ResetState()
        {
            parsingDataBufferLength = 0;
            payloadBufferLength = 0;
            dataStringLength = 0;
            failedCompareCount = 0;
        }

        void IncrementFirmwareIndex()
        {
            ++firmwareIndex;
        }

        void IncrementParsingDataBufferLength()
        {
            ++parsingDataBufferLength;
        }

        void IncrementPayloadBufferLength()
        {
            ++payloadBufferLength;
        }
    };
    TParsingState parsingState;
}

BatteryFlasher::BatteryFlasher(I2C* i2cInterfaceIn)
    : m_i2c(i2cInterfaceIn)
    , m_subsystem(OROV_TRIDENT_SUBSYSTEM_ID_BATTERY_FLASHER, "BATT_FLASH")
    , m_flasherStatus{
        xtimer_now(),
        OROV_SUBSYSTEM_STATE_UNKNOWN,
        OROV_BATT_FLASH_SUBSTATE_INIT,
        16,
        OROV_BATT_FLASH_NO_ERROR }
    , m_voltage_mv(0)
    , m_avgCurrent_ma(0)
    , m_insCurrent_ma(0)
    , m_avgPower_mw(0)
    , m_stateOfCharge_pct(0)
    , m_remainingCapacity_mah(0)
    , m_batteryTemperature_k(0)
    , m_flags(0)
    , m_firmwareVersion()
{

}

void BatteryFlasher::SendStatusUpdate()
{
    LOG_DEBUG("Sending status \n\r");
    m_flasherStatus.timestamp = xtimer_now();
    mavlink_msg_orov_batt_flash_status_send_struct(CommLink::MAIN_CHANNEL, &m_flasherStatus);
    

    if(m_flasherStatus.substate == OROV_BATT_FLASH_SUBSTATE_SUCCESS)
    {
        // Send out the fuel gauge state
        ReadFuelgaugeValues();
        m_fuelgaugeStatus.timestamp 		= xtimer_now();
        m_fuelgaugeStatus.voltage 		    = m_voltage_mv;
        m_fuelgaugeStatus.avg_current 	    = m_avgCurrent_ma;
        m_fuelgaugeStatus.current 		    = m_insCurrent_ma;
        m_fuelgaugeStatus.avg_power 		= m_avgPower_mw;
        m_fuelgaugeStatus.remaining_pct 	= m_stateOfCharge_pct;
        m_fuelgaugeStatus.remaining_mah 	= m_remainingCapacity_mah;
        m_fuelgaugeStatus.batt_temp 		= static_cast<float>( m_batteryTemperature_k ) - 273.15f;
        m_fuelgaugeStatus.flags 			= m_flags;
        mavlink_msg_orov_fuelgauge_status_send_struct(CommLink::MAIN_CHANNEL, &m_fuelgaugeStatus);

        LOG_DEBUG("Voltage %u\n\r", m_voltage_mv);

        // Check if it enabled
        bool isITEnabled = true;
        auto ret = bq34z100_interface::IsITEnabled(m_i2c, isITEnabled);
        if(ret)
        {
            LOG_DEBUG("FAILED TO CHECK bq34z100_interface::IsITEnabled");
        }
        if(!isITEnabled)
        {
            LOG_DEBUG("IT TRACKING IS NOT ENABLED");
        }

        bool isSealed = true;
        ret = bq34z100_interface::IsSealed(m_i2c, isSealed);
        if(ret)
        {
            LOG_DEBUG("FAILED TO CHECK bq34z100_interface::IsSealed");
        }
        if(!isSealed)
        {
            LOG_DEBUG("DEVICE IS NOT SEALED");
        }
    }
}
void BatteryFlasher::SetFlasherMainState(const OROV_SUBSYSTEM_STATE& stateIn)
{
    m_flasherStatus.state = stateIn;

    // Send out a status update when we switch main states
    SendStatusUpdate();
}
void BatteryFlasher::SetFlasherSubstate(const OROV_BATT_FLASH_SUBSTATE& stateIn)
{
    m_flasherStatus.substate = stateIn;

    // Send out a status update when we switch substates
    SendStatusUpdate();
}
void BatteryFlasher::SetFlasherLastError(const OROV_BATT_FLASH_ERROR& errorIn)
{
    m_flasherStatus.last_error = errorIn;
}

void BatteryFlasher::Initialize()
{
    LOG_DEBUG("BATT_FLASH: Init \n\r");
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_INIT);

    // Reset timers
    flashWait.timer.Reset();
    statusUpdateTimer.Reset();

    // Update the state
    m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_INITIALIZED);
    m_subsystem.TransitionSubstate(ESubstate::READY_FOR_FLASH_COMMAND);
    
    LOG_DEBUG("BATT_FLASH: Finished Init \n\r");
}

void BatteryFlasher::Post()
{
    LOG_DEBUG("BATT_FLASH: POSTing \n\r");
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_POSTING);

    // Posting for the app consists of verifying that the i2c is ready
	m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_POSTING);
	m_subsystem.SetPostResult(OROV_POST_RESULT_WAITING);

    // Go through the precheck process to verify that the device is ready
    int postCount = 0;
    bool postResult = false;

    do
    {
        postResult = PostAttempt();
        ++postCount;
    }
    while(!postResult && postCount < 3);

    if(postResult)
    {
        LOG_DEBUG("BATT_FLASH: POST Success \n\r");

        // Success
        m_subsystem.SetPostResult(OROV_POST_RESULT_SUCCESS);

        // Update state, and standby for signal to start flashing
        m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
        m_subsystem.TransitionSubstate(ESubstate::READY_FOR_FLASH_COMMAND);
    }
    else
    {
        LOG_DEBUG("BATT_FLASH: Failed POST \n\r");

        // Failure
		m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
		m_subsystem.TransitionSubstate(ESubstate::FAILURE);
        SetFlasherLastError(OROV_BATT_FLASH_ERROR_FAILED_PRECHECK);
    }
}

bool BatteryFlasher::DevicePrecheck()
{
    LOG_DEBUG("BATT_FLASH: Device PreCheck \n\r");
    // Check to see if the i2c is available
    if(m_i2c->GetState() != EI2CState::READY)
    {
        LOG_DEBUG("BATT_FLASH: I2C is not ready!\n\r");
        m_subsystem.SetLastErrorCode(ERetCode::ERROR_IO);
        SetFlasherLastError(OROV_BATT_FLASH_ERROR_I2C_NOT_READY);
        return false;
    }

    // Try to read the firmware version to make sure the battery chip is ready to be flashed and in a good state
    auto ret = bq34z100_interface::ReadFirmwareVersion(m_i2c, m_firmwareVersion);
    if(ret != i2c_retcode_t::I2C_RETCODE_SUCCESS)
    {
        // Failed to read firmware version
        LOG_DEBUG("BATT_FLASH: FAILED DEVICE PRECHECK \n\r");
        LOG_DEBUG("RET CODE: %u\n\r", ret);
        SetFlasherLastError(OROV_BATT_FLASH_ERROR_FAILED_PRECHECK);
        return false;
    }
    
    LOG_DEBUG("BATT_FLASH: Device Precheck Success \n\r");
    LOG_DEBUG("BATT_FLASH: Firmware Version: %u %u \n\r", m_firmwareVersion[0], m_firmwareVersion[1]);
    return true;
}

bool BatteryFlasher::PostAttempt()
{
    // Wait
    LOG_DEBUG("BATT_FLASH: POST attempt. \n\r");
	
    // Wait 250ms
	xtimer_usleep( 250000 );

    // Go through the device precheck steps
    auto ret = DevicePrecheck();
    return ret;
}

const char* BatteryFlasher::GetSubstate(const uint8_t& substateIn)
{
    switch(substateIn)
    {
        case(static_cast<uint8_t>(ESubstate::COMPARING)):
        {
            return "COMPARING";
        }
        case(static_cast<uint8_t>(ESubstate::FINISHED_PARSING)):
        {
            return "FINISHED_PARSING";
        }
        case(static_cast<uint8_t>(ESubstate::PARSING)):
        {
            return "PARSING";
        }
        case(static_cast<uint8_t>(ESubstate::READY_FOR_FLASH_COMMAND)):
        {
            return "READY_FOR_FLASH_COMMAND";
        }
        case(static_cast<uint8_t>(ESubstate::WAITING)):
        {
            return "WAITING";
        }
        case(static_cast<uint8_t>(ESubstate::WRITING)):
        {
            return "WRITING";
        }
        case(static_cast<uint8_t>(ESubstate::FLASHING_SUCCESS)):
        {
            return "FLASHING_SUCCESS";
        }
    }
    return "UNKNOWN";
}

void BatteryFlasher::Update()
{
    // Handle incoming messages
    HandleMessages();

    // Our current substate
    auto state = m_subsystem.GetState();

    // Hearbeat status updates at 1Hz
    if(statusUpdateTimer.HasElapsed(statusUpdateRate))
    {
        SendStatusUpdate();
    }

    switch(state)
    {
		case OROV_SUBSYSTEM_STATE_ACTIVE:
        {
            UpdateActive(); 
            break; 
        }

		default: 
		{	// Stay in the active state Move to the standby state. These states aren't supported
			m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
			m_subsystem.TransitionSubstate(ESubstate::FAILURE);
			break;
		}
	}
}

void BatteryFlasher::HandleMessages()
{
    int messageId = CommLink::GetChannel(CommLink::MAIN_CHANNEL)->GetCurrentMessageID();
    mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();

    switch(messageId)
    {
        case(MAVLINK_MSG_ID_OROV_BATT_FLASH_CONTROL):
        {
            auto cmd = mavlink_msg_orov_batt_flash_control_get_cmd(message);
            switch(cmd)
            {
                case(OROV_BATT_FLASH_COMMAND_START):
                {
                    m_startFlashing = true;
                    break;
                }
                case(OROV_BATT_FLASH_COMMAND_RESET):
                {
                    m_startFlashing = true;
                    m_shouldSkipEnterROM = true;
                    break;
                }
                default:
                {
                    break;
                }
            }
            break;
        }
        default:
        {
            // Not us, pass
            break;
        }
    }
}

void BatteryFlasher::UpdateActive()
{
    switch(m_subsystem.GetSubstate())
    {
        case(static_cast<uint8_t>(ESubstate::COMPARING)):
        {
            if(HandleCompare() != ERetCode::SUCCESS)
            {
                // TODO: Failed a compare, increment statistics and/or handle the error
                LOG_DEBUG("BATT_FLASH: FAILED COMPARE \n\r");
                SetFlasherLastError(OROV_BATT_FLASH_ERROR_FAILED_COMPARE);
                m_subsystem.TransitionSubstate(ESubstate::FAILURE);

                // We are already in ROM mode, skip it this next time we attempt to flash
                SkipROMMode();
            }

            // Transition back to the parsing state
            m_subsystem.TransitionSubstate(ESubstate::PARSING);
            break;
        }
        case(static_cast<uint8_t>(ESubstate::PARSING)):
        {
            // Parse the firmware file
            if(HandleParse() != ERetCode::SUCCESS)
            {
                // TODO: Parsing failed, increment statistics and/or handle the error
                LOG_DEBUG("BATT_FLASH: FAILED PARSE \n\r");
                SetFlasherLastError(OROV_BATT_FLASH_ERROR_FAILED_PARSE);
                m_subsystem.TransitionSubstate(ESubstate::FAILURE);

                // We are already in ROM mode, skip it this next time we attempt to flash
                SkipROMMode();
            }
            break;
        }
        case(static_cast<uint8_t>(ESubstate::WAITING)):
        {
            // Check to see if we have waited long enough
            if(flashWait.timer.HasElapsed(flashWait.waitTime_us))
            {
                // If we have waited long enough, transition back to the parsing state
                m_subsystem.TransitionSubstate(ESubstate::PARSING);
            }
            break;
        }
        case(static_cast<uint8_t>(ESubstate::WRITING)):
        {
            if(HandleWrite() != ERetCode::SUCCESS)
            {
                // TODO: Failed a write, increment statistics and/or handle the error
                LOG_DEBUG("BATT_FLASH: FAILED Write \n\r");
                SetFlasherLastError(OROV_BATT_FLASH_ERROR_FAILED_WRITE);
                m_subsystem.TransitionSubstate(ESubstate::FAILURE);

                // We are already in ROM mode, skip it this next time we attempt to flash
                SkipROMMode();
            }

            // Transition back to the parsing state
            m_subsystem.TransitionSubstate(ESubstate::PARSING);
            break;
        }
        case(static_cast<uint8_t>(ESubstate::FINISHED_PARSING)):
        {
            LOG_DEBUG("BATT_FLASH: FINISHED PARSING: \n\r");

            if(FinishFlashing() != ERetCode::SUCCESS)
            {
                // TODO: Failed to finish the flashing process
                LOG_DEBUG("FAILED TO FINSIH FLASHING PROCESS \n\r");
                m_subsystem.TransitionSubstate(ESubstate::FAILURE);
            }
            
            m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
			m_subsystem.TransitionSubstate(ESubstate::FLASHING_SUCCESS);

            break;
        }
        case(static_cast<uint8_t>(ESubstate::READY_FOR_FLASH_COMMAND)):
        {
            // Waiting for flash command
            if(m_startFlashing)
            {
                // Preform a precheck before flashing to make sure we are ready to flash
                auto ret = DevicePrecheck();
                if(ret)
                {
                    // Precheck was successful, start flashing process
                    StartFlashing();
                    m_startFlashing = false;
                }
                else
                {
                    // Precheck was failed, do not continue with flashing
                    m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
                    m_subsystem.TransitionSubstate(ESubstate::FAILURE);
                }
            }
            else
            {
                // Transition state once and report to host
                if(m_flasherStatus.substate != OROV_BATT_FLASH_SUBSTATE_READY_FOR_FLASH)
                {
                    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_READY_FOR_FLASH);
                }
            }

            break;
        }
        case(static_cast<uint8_t>(ESubstate::FLASHING_SUCCESS)):
        {
            // Transition state once and report to host
            if(m_flasherStatus.substate != OROV_BATT_FLASH_SUBSTATE_SUCCESS)
            {
                SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_SUCCESS);
            }

            break;
        }
        case(static_cast<uint8_t>(ESubstate::FAILURE)):
        {
            // Transition state once and report to host
            if(m_flasherStatus.substate != OROV_BATT_FLASH_SUBSTATE_FAILED)
            {
                SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_FAILED);
                m_subsystem.TransitionSubstate(ESubstate::READY_FOR_FLASH_COMMAND);
            }
            break;
        }
        default:
        {
            // Should never get here, pass
            break;
        }
    }
}

void BatteryFlasher::ReadFuelgaugeValues()
{
    // Send out values from the fuel gauge to verify we are operational
	i2c_retcode_t ret;
	ret = bq34z100_interface::ReadVoltage( m_i2c, m_voltage_mv );						if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadAverageCurrent( m_i2c, m_avgCurrent_ma );			if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadInstantCurrent( m_i2c, m_insCurrent_ma );			if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadAveragePower( m_i2c, m_avgPower_mw );				if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadStateOfCharge( m_i2c, m_stateOfCharge_pct );			if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadRemainingCapacity(m_i2c, m_remainingCapacity_mah );	if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadTemperature( m_i2c, m_batteryTemperature_k );		if( ret ){  goto fail; }
	ret = bq34z100_interface::ReadFlags( m_i2c, m_flags );								if( ret ){  LOG_DEBUG("FAILED TO READ FLAGS \n\r");goto fail; }

	// Success
    return;
fail:
	// Failure
    // TODO Add last reason
    LOG_DEBUG("FAILED TO READ VALUES \n\r");
    m_subsystem.TransitionSubstate(ESubstate::FAILURE);    

    return;
}

void BatteryFlasher::StartFlashing()
{
    LOG_DEBUG("BATT_FLASH: Starting flashing process \n\r");

    // Transition into the active parsing state
    m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
    m_subsystem.TransitionSubstate(ESubstate::PARSING);

    // Reset our state
    parsingState.ResetState();
}

BatteryFlasher::ERetCode BatteryFlasher::HandleCompare()
{
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_COMPARING);

    auto ret = ERetCode::SUCCESS;
    // Compare reads from an address, and then does a comparison

    // Compare command structure: 
    // C: [i2caddr] [reg address] [data to compre]
    // Data will contain the buffer + 1 for the new line
    // Add new line to buffer so strtok works
    auto newLineIndex = parsingState.parsingDataBufferLength + 1;
    if( newLineIndex > bq34z100_interface::MAX_DATA_BYTES_PER_LINE)
    {
        return ERetCode::ERROR_INDEX_OVERFLOW;
    }
    parsingState.parsingDataBuffer[newLineIndex] = '\n';

    // Convert to char for strtok_r
    memcpy(parsingState.dataString, parsingState.parsingDataBuffer, newLineIndex);

    // A placeholder to walk the pointer through the data string
    char* tokenPointer = nullptr;

    // W: 
    char* command;
    
    // The i2c address
    char* i2cAddress = nullptr;

    // The register address
    char* regAddress = nullptr;

    // Parse the first part of the data string, separating on spaces
    command = strtok_r(parsingState.dataString, " ", &tokenPointer);
    if(!command)
    {
        // Didn't get a string, report error
        ret = ERetCode::ERROR_COMPARE_PARSE;
        return ret;  
    }

    i2cAddress = strtok_r(NULL, " ", &tokenPointer);
    regAddress = strtok_r(NULL, " ", &tokenPointer);

    // Parse the rest of the payload
    char* payloadByte;

    while((payloadByte = strtok_r(tokenPointer, " ", &tokenPointer)))
    {
        // Convert
        auto byte = static_cast<uint8_t>(strtol(payloadByte, NULL, 16));

        // Add to the buffer
        auto payloadIndex = parsingState.payloadBufferLength;
        
        parsingState.payloadBuffer[payloadIndex] = byte;
        parsingState.IncrementPayloadBufferLength();
    }

    // Convert to data types i2c wrapper expects
    auto i2cAddressOut = static_cast<uint8_t>(strtol(i2cAddress, NULL, 16));
    i2cAddressOut = (i2cAddressOut >> 1); // bit shift to left by one, as per ti data sheet

    auto regAddressOut = static_cast<uint8_t>(strtol(regAddress, NULL, 16));
    ignore(regAddressOut);

    // Read from the register
    auto retCode = m_i2c->ReadRegisterBytes(i2cAddressOut, regAddressOut, parsingState.dataToCompare, parsingState.payloadBufferLength);
    if(retCode)
    {
        LOG_DEBUG("FAILED TO READ on i2c address %u and reg address %u with length %u\n\r", i2cAddressOut, regAddressOut, parsingState.payloadBufferLength);
        ret = ERetCode::ERROR_IO;

        return ret;
    }

    // Compare
    for(size_t i = 0; i < parsingState.payloadBufferLength; ++i)
    {
        if(parsingState.dataToCompare[i] != parsingState.payloadBuffer[i])
        {
            ret = ERetCode::ERROR_FAILED_COMPARE;
        }
    }
    return ret;
}

BatteryFlasher::ERetCode BatteryFlasher::HandleParse()
{
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_PARSING);
    auto ret = ERetCode::SUCCESS;

    // If we have reached the end of the firmware file, move into the FINSIHED_PARSING state
    if(parsingState.firmwareIndex == bq34z100_interface::BATTERY_FIRMWARE_LENGTH)
    {
        m_subsystem.TransitionState(OROV_SUBSYSTEM_STATE_ACTIVE);
        m_subsystem.TransitionSubstate(ESubstate::FINISHED_PARSING);
        return ret;
    }

    // Reset the data buffer for firmware parsing
    parsingState.ResetState();
    bool isNewLine = false;

    // First byte in the data will be the command (wait, write, compare, comment)
    auto command = k_battery[parsingState.firmwareIndex];

    while(!isNewLine)
    {
        // TODO: Add check for bounds
        
        // Get the data from the firmware
        auto firmwareDataByte = k_battery[parsingState.firmwareIndex];
        parsingState.IncrementFirmwareIndex();

        // Check if this is a new line
        if(firmwareDataByte == bq34z100_interface::firmware_keycodes::NEW_LINE)
        {
            // break out of the while loop on the next pass
            isNewLine = true;
        }
        // add data to the array
        parsingState.parsingDataBuffer[parsingState.parsingDataBufferLength] = firmwareDataByte;
        parsingState.IncrementParsingDataBufferLength();
    }

    if(command != bq34z100_interface::firmware_keycodes::UNSEALED_MARK && m_shouldSkipEnterROM)
    {
        return ret;
    }
    
    // Act on the command
    switch(command)
    {
        case(bq34z100_interface::firmware_keycodes::COMMENT):
        {
            // TODO: Stub to act on comments, right now just pass
            m_subsystem.TransitionSubstate(ESubstate::PARSING);
            break;
        }
        case(bq34z100_interface::firmware_keycodes::COMPARE):
        {
            // Transition to handling compares
            m_subsystem.TransitionSubstate(ESubstate::COMPARING);
            break;
        }
        case(bq34z100_interface::firmware_keycodes::WAIT):
        {
            // Parse the wait payload
            uint32_t waitTime_ms = 0;
            auto ret = HandleWait(waitTime_ms);
            if(ret != ERetCode::SUCCESS)
            {
                // TODO: Failed to parse a wait command
                LOG_DEBUG("BATT_FLASH: Failed to parse wait command \n\r");
                SetFlasherLastError(OROV_BATT_FLASH_ERROR_FAILED_WAIT);
                m_subsystem.TransitionSubstate(ESubstate::FAILURE);

                // We are already in ROM mode, skip it this next time we attempt to flash
                SkipROMMode();

            }
            else
            {
                // Successful wait parse, convert to us for xtimer
                flashWait.waitTime_us = ConvertMillisecondsToMicroSeconds<uint32_t>(waitTime_ms);

                //Transition into waiting
                flashWait.timer.Reset();
                m_subsystem.TransitionSubstate(ESubstate::WAITING);
            }

            break;
        }
        case(bq34z100_interface::firmware_keycodes::WRITE):
        {
            // Transition to handling compares
            m_subsystem.TransitionSubstate(ESubstate::WRITING);
            break;
        }
        case(bq34z100_interface::firmware_keycodes::UNSEALED_MARK):
        {
            LOG_DEBUG("GOT AN UNSEAL MARK THE DEVICE SHOULD BE UNSEALED \n\r");
            if(m_shouldSkipEnterROM)
            {
                // We should reflash the device starting after this line
                m_shouldSkipEnterROM = false;
            }
            break;
        }
        default:
        {
            // Unrecongnized command, this is an error
            // TODO: Increment error statistics
            ret = ERetCode::ERROR_UNRECOGNIZED_FIRMWARE_COMMAND;
            break;
        }
    }

    return ret;
}

BatteryFlasher::ERetCode BatteryFlasher::HandleWrite()
{
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_WRITING);
    auto ret = ERetCode::SUCCESS;

    // Write command structure:
    // W: [i2caddr][regaddr] [payload]

    // First three chars (W,:, ,) are diregarded
    // Data will contain the buffer + 1 for the new line

    // Add new line to buffer so strtok works
    auto newLineIndex = parsingState.parsingDataBufferLength + 1;
    if( newLineIndex > bq34z100_interface::MAX_DATA_BYTES_PER_LINE)
    {
        return ERetCode::ERROR_INDEX_OVERFLOW;
    }
    parsingState.parsingDataBuffer[newLineIndex] = '\n';

    // Convert to char for strtok_r
    memcpy(parsingState.dataString, parsingState.parsingDataBuffer, newLineIndex);

    // A placeholder to walk the pointer through the data string
    char* tokenPointer;

    // W: 
    char* command;
    
    // The i2c address
    const char* i2cAddress;

    // The register address
    char* regAddress;

    // Parse the first part of the data string, separating on spaces
    command = strtok_r(parsingState.dataString, " ", &tokenPointer);
    if(!command)
    {
        // Didn't get a string, report error
        ret = ERetCode::ERROR_WRITE_PARSE;
        return ret;
    }

    i2cAddress = strtok_r(NULL, " ", &tokenPointer);
    regAddress = strtok_r(NULL, " ", &tokenPointer);

    // Parse the rest of the payload
    char* payloadByte;

    while((payloadByte = strtok_r(tokenPointer, " ", &tokenPointer)))
    {
        // Convert
        auto byte = static_cast<uint8_t>(strtol(payloadByte, NULL, 16));

        // Add to the buffer
        auto payloadIndex = parsingState.payloadBufferLength;

        parsingState.payloadBuffer[payloadIndex] = byte;
        parsingState.IncrementPayloadBufferLength();
    }

    // Convert to data types i2c wrapper expects
    auto i2cAddressOut = static_cast<uint8_t>(strtol(i2cAddress, NULL, 16));
    i2cAddressOut = (i2cAddressOut >> 1); // bit shift to left by one, as per ti data sheet

    auto regAddressOut = static_cast<uint8_t>(strtol(regAddress, NULL, 16));
    ignore(regAddressOut);
    //LOG_DEBUG("BATT_FLASH: Writing to i2c address %u and reg address %u\n\r", i2cAddressOut, regAddressOut);

    // Write the data over i2c
    auto retCode = m_i2c->WriteRegisterBytes(i2cAddressOut, regAddressOut, parsingState.payloadBuffer, parsingState.payloadBufferLength);
    if(retCode)
    {
        // TODO: i2c error. handle it
        LOG_DEBUG("Failed to write over i2c \n\r");
        ret = ERetCode::ERROR_IO;
    }

    return ret;
}

BatteryFlasher::ERetCode BatteryFlasher::HandleWait(uint32_t& waitTime)
{
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_WAITING);
    auto ret = ERetCode::SUCCESS;
    
    // Wait command structure:
    // X: [wait in ms]
    // Token is first three chars [X: ]

    // Add new line to buffer so strtok works
    auto newLineIndex = parsingState.parsingDataBufferLength + 1;
    if( newLineIndex > bq34z100_interface::MAX_DATA_BYTES_PER_LINE)
    {
        ret = ERetCode::ERROR_INDEX_OVERFLOW;
        return ret;
    }
    parsingState.parsingDataBuffer[newLineIndex] = '\n';
    
    // Convert to char for strtok_r
    memcpy(parsingState.dataString, parsingState.parsingDataBuffer, newLineIndex);

    // A placeholder to walk the pointer through the data string
    char* tokenPointer;

    // X: 
    char* command;

    // Parse the first part of the data string, separating on spaces
    command = strtok_r(parsingState.dataString, " ", &tokenPointer);
    if(!command)
    {
        // Didn't get a string, report error
        ret = ERetCode::ERROR_WAIT_TIME_PARSE;
        return ret;
    }

    // Get the wait time
    char* waitTimePayload;
    
    while((waitTimePayload = strtok_r(tokenPointer, " ", &tokenPointer)))
    {
        // Convert to a ms integer value
        waitTime = static_cast<uint32_t>(atoi(waitTimePayload));
        if(waitTime > 0)
        {
            // If we get data, break out
            break;
        }  
    }

    if(waitTime == 0)
    {
        // TODO: Bad parse, report
        ret = ERetCode::ERROR_WAIT_TIME_PARSE;
    }

    return ret;
}

BatteryFlasher::ERetCode BatteryFlasher::FinishFlashing()
{
    LOG_DEBUG("BATT_FLASH: Finished flashing \n\r");

    // Reset
    LOG_INFO("BATT_FLASH: Reseting the device \n\r");
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_RESETING_DEVICE);
    auto ret = bq34z100_interface::Reset(m_i2c, bq34z100_interface::shifted_register_address::DEVICE_ADDRESS);
    if(ret)
    {
        LOG_DEBUG("BATT_FLASH: Failed to reset the device \n\r");
        return ERetCode::ERROR_FAILED_RESET;
    }
    xtimer_sleep(1);
    
    // Enable it tracking
    LOG_INFO("BATT_FLASH: Enabling IT Tracking \n\r");
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_ENABLING_IT_TRACKING);
    ret = bq34z100_interface::ITEnable(m_i2c);
    if(ret)
    {
        LOG_DEBUG("BATT_FLASH: Failed to enable IT Tracking \n\r");
        return ERetCode::ERROR_FAILED_IT_TRACKING;
    }
    xtimer_sleep(1);

    // seal the device
    SetFlasherSubstate(OROV_BATT_FLASH_SUBSTATE_SEALING);
    LOG_INFO("BATT_FLASH: Sealing the device\n\r");
    ret = bq34z100_interface::Seal(m_i2c);
    if(ret)
    {
        LOG_DEBUG("BATT_FLASH: Failed to SEAL the device \n\r");
        return ERetCode::ERROR_FAILED_SEALING;
    }

    return ERetCode::SUCCESS;

}
