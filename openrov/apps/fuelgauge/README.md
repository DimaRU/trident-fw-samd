# Prereqs:
- gcc-arm-none-eabi
- gdb-arm-none-eabi

# Build:
- make clean
- make

This application serves as a flasher for the bq34z100g fuel gauge chip