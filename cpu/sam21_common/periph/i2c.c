/*
 * Copyright (C) 2014 CLENET Baptiste
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup  cpu_samd21
 * @{
 * @file
 * @brief       Low-level I2C driver implementation
 * @author      Baptiste Clenet <baptiste.clenet@xsoen.com>
 * @author      Thomas Eichinger <thomas.eichinger@fu-berlin.de>
 * @}
 */

#include <stdint.h>

#include "cpu.h"
#include "board.h"
#include "mutex.h"
#include "periph_conf.h"
#include "periph/i2c.h"

#include "sched.h"
#include "thread.h"

#include <log.h>

#define ENABLE_DEBUG    (0)
#include "debug.h"

/* guard file in case no I2C device is defined */
#if I2C_NUMOF

#define SAMD21_I2C_TIMEOUT  (20000)

#define BUSSTATE_UNKNOWN SERCOM_I2CM_STATUS_BUSSTATE(0)
#define BUSSTATE_IDLE SERCOM_I2CM_STATUS_BUSSTATE(1)
#define BUSSTATE_OWNER SERCOM_I2CM_STATUS_BUSSTATE(2)
#define BUSSTATE_BUSY SERCOM_I2CM_STATUS_BUSSTATE(3)

/* static function definitions */
static void _i2c_poweron(SercomI2cm *sercom);
static void _i2c_poweroff(SercomI2cm *sercom);

static inline int _start(SercomI2cm *dev, uint8_t address, uint8_t rw_flag);
static inline int _write(SercomI2cm *dev, const uint8_t *data, int length);
static inline int _read(SercomI2cm *dev, uint8_t *data, int length);
static inline void _stop(SercomI2cm *dev);

/**
 * @brief Array holding one pre-initialized mutex for each I2C device
 */
static mutex_t locks[] = {
#if I2C_0_EN
    [I2C_0] = MUTEX_INIT,
#endif
#if I2C_1_EN
    [I2C_1] = MUTEX_INIT,
#endif
#if I2C_2_EN
    [I2C_2] = MUTEX_INIT
#endif
#if I2C_3_EN
    [I2C_3] = MUTEX_INIT
#endif
};

enum EBusCommand
{
    BUS_COMMAND_NO_ACTION 		= 0x0,
    BUS_COMMAND_REPEAT_START	= 0x1,
    BUS_COMMAND_BYTE_READ		= 0x2,
    BUS_COMMAND_STOP			= 0x3
};

void SyncReset_I2C(SercomI2cm *sercom)
{
	// Sync after setting CTRLA.SWRST
	while( sercom->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_SWRST );
}

void SyncEnable_I2C(SercomI2cm *sercom)
{
	// Sync after setting CTRLA.ENABLE
	while( sercom->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_ENABLE );
}

void SyncSysOp_I2C(SercomI2cm *sercom)
{
	// Sync after:
	// 	Writing to STATUS.BUSSTATE in Master Mode
	// 	Writing to ADDR.ADDR in Master Mode
	//	Writing to DATA in Master Mode
	while( sercom->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_SYSOP );
}

void SyncBusy_I2C(SercomI2cm *sercom)
{
	// Sync after all types of sync events are complete
	while( sercom->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_MASK );
}

void SendBusCommand_I2C( SercomI2cm *sercom, uint8_t commandIn )
{
	// Set CTRLB.CMD to issue the specified master operation
    sercom->CTRLB.reg |= (SERCOM_I2CM_CTRLB_CMD( commandIn ));
    while( sercom->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_SYSOP );
}

int i2c_init_master(i2c_t dev, i2c_speed_t speed)
{
    SercomI2cm *I2CSercom = 0;
    gpio_t pin_scl = 0;
    gpio_t pin_sda = 0;
    gpio_mux_t mux;
    uint32_t clock_source_speed = 0;
    uint8_t sercom_gclk_id = 0;
    uint8_t sercom_gclk_id_slow = 0;

    uint32_t timeout_counter = 0;
    int32_t tmp_baud;

    switch (dev) {
#if I2C_0_EN
        case I2C_0:
            I2CSercom = &I2C_0_DEV;
            pin_sda = I2C_0_SDA;
            pin_scl = I2C_0_SCL;
            mux = I2C_0_MUX;
            clock_source_speed = CLOCK_CORECLOCK;
            sercom_gclk_id = I2C_0_GCLK_ID;
            sercom_gclk_id_slow = I2C_0_GCLK_ID_SLOW ;
            break;
#endif
        default:
            DEBUG("I2C_RETCODE_ERROR_BAD_DEV\n");
            return I2C_RETCODE_ERROR_BAD_DEV;
    }

    // This disables the i2c peripheral
    i2c_poweroff(dev);

    // Setup pinmux
    gpio_init_mux(pin_sda, mux);
    gpio_init_mux(pin_scl, mux);

    // Turn on power manager for sercom peripheral
    PM->APBCMASK.reg |= (PM_APBCMASK_SERCOM0 << (sercom_gclk_id - GCLK_CLKCTRL_ID_SERCOM0_CORE_Val));

    // Drive I2C with GCLK0
    GCLK->CLKCTRL.reg = (GCLK_CLKCTRL_CLKEN |
                         GCLK_CLKCTRL_GEN_GCLK0 |
                         GCLK_CLKCTRL_ID(sercom_gclk_id));
    while (GCLK->STATUS.bit.SYNCBUSY) {}

    GCLK->CLKCTRL.reg = (GCLK_CLKCTRL_CLKEN |
                         GCLK_CLKCTRL_GEN_GCLK0 |
                         GCLK_CLKCTRL_ID(sercom_gclk_id_slow));
    while (GCLK->STATUS.bit.SYNCBUSY) {}

    // Reset I2C
    I2CSercom->CTRLA.reg = SERCOM_I2CS_CTRLA_SWRST;
    while(I2CSercom->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_MASK) {}

    // Set up to operate in master mode
    I2CSercom->CTRLA.reg = SERCOM_I2CM_CTRLA_MODE_I2C_MASTER;

    /* Find and set baudrate. Read speed configuration. Set transfer
     * speed: SERCOM_I2CM_CTRLA_SPEED(0): Standard-mode (Sm) up to 100
     * kHz and Fast-mode (Fm) up to 400 kHz */

    int32_t target_baud = 0;
    switch (speed) {
        case I2C_SPEED_NORMAL:
            target_baud = 100000;
            tmp_baud = (int32_t)( ( clock_source_speed - ( 10 * target_baud ) - ( target_baud * clock_source_speed * ( 150 * 0.000000001 ) ) ) / ( 2 * target_baud ) );
            if (tmp_baud < 255 && tmp_baud > 0) {
                I2CSercom->CTRLA.reg |= SERCOM_I2CM_CTRLA_SPEED(0);
                I2CSercom->BAUD.reg = SERCOM_I2CM_BAUD_BAUD(tmp_baud);
            }
            break;
        case I2C_SPEED_FAST:
            target_baud = 400000;
            tmp_baud = (int32_t)( ( clock_source_speed - ( 10 * target_baud ) - ( target_baud * clock_source_speed * ( 150 * 0.000000001 ) ) ) / ( 2 * target_baud ) );
            if (tmp_baud < 255 && tmp_baud > 0) {
                I2CSercom->CTRLA.reg |= SERCOM_I2CM_CTRLA_SPEED(0);
                I2CSercom->BAUD.reg = SERCOM_I2CM_BAUD_BAUD(tmp_baud);
            }
            break;
        case I2C_SPEED_HIGH:
            tmp_baud = (int32_t)(((clock_source_speed + (2*(3400000)) - 1) / (2*(3400000))) - 1);
            if (tmp_baud < 255 && tmp_baud > 0) {
                I2CSercom->CTRLA.reg |= SERCOM_I2CM_CTRLA_SPEED(2);
                I2CSercom->BAUD.reg =SERCOM_I2CM_BAUD_HSBAUD(tmp_baud);
            }
            break;
        default:
            DEBUG("I2C_RETCODE_ERROR_BAD_BAUDRATE\n");
            return I2C_RETCODE_ERROR_BAD_BAUDRATE;
    }

    /* ENABLE I2C MASTER */
    i2c_poweron(dev);

    // Set bus state to idle. Wait for it to go idle.
    I2CSercom->STATUS.reg = BUSSTATE_IDLE;
    while ((I2CSercom->STATUS.reg & SERCOM_I2CM_STATUS_BUSSTATE_Msk) != BUSSTATE_IDLE) 
    {
        if (++timeout_counter >= SAMD21_I2C_TIMEOUT) 
        {
            return I2C_RETCODE_ERROR_INVALID_BUS_STATE;
        }
    }

    return I2C_RETCODE_SUCCESS;
}

int i2c_acquire(i2c_t dev)
{
    if (dev >= I2C_NUMOF) {
        return I2C_RETCODE_ERROR_BAD_DEV;
    }
    mutex_lock(&locks[dev]);
    return I2C_RETCODE_SUCCESS;
}

int i2c_release(i2c_t dev)
{
    if (dev >= I2C_NUMOF) {
        return I2C_RETCODE_ERROR_BAD_DEV;
    }
    mutex_unlock(&locks[dev]);
    return I2C_RETCODE_SUCCESS;
}

// Read commands return:
// On Error: Error code (negative)
// On success: 0 to N bytes read/written

// Base read command
int i2c_read_bytes(i2c_t dev, uint8_t address, void *data, int length)
{
    SercomI2cm *i2c;

    switch (dev) {
#if I2C_0_EN
        case I2C_0:
            i2c = &I2C_0_DEV;
            break;
#endif
        default:
            return I2C_RETCODE_ERROR_BAD_DEV;
    }

    int ret = I2C_RETCODE_SUCCESS;

    // Start transaction and send slave address + read bit
    ret = _start( i2c, address, I2C_FLAG_READ );
    if( ret < 0 ){ return ret; }

    // Read data to register
    ret = _read(i2c, data, length);
    if( ret < 0 ){ return ret; }

    // End transaction
    _stop( i2c );

    /* return number of bytes sent */
    return ret;
}

int i2c_read_byte(i2c_t dev, uint8_t address, void *data)
{
    return i2c_read_bytes(dev, address, data, 1);
}

int i2c_read_regs(i2c_t dev, uint8_t address, uint8_t reg, void *data, int length)
{
    SercomI2cm *i2c;

    switch (dev) {
#if I2C_0_EN
        case I2C_0:
            i2c = &I2C_0_DEV;
            break;
#endif
        default:
            return I2C_RETCODE_ERROR_BAD_DEV;
    }

    int ret = I2C_RETCODE_SUCCESS;

    // Start transaction and send slave address + read bit
    ret = _start( i2c, address, I2C_FLAG_WRITE );
    if( ret < 0 ){ return ret; }

    // Write register address/command code
    ret = _write(i2c, &reg, 1);
    if( ret < 0 ){ return ret; }

    // Read bytes and return result
    return i2c_read_bytes( dev, address, data, length );
}

int i2c_read_reg(i2c_t dev, uint8_t address, uint8_t reg, void *data)
{
    return i2c_read_regs( dev, address, reg, data, 1 );
}

// Write commands return:
// On Error: Error code (negative)
// On success: 0 to N bytes read/written

// Base write method
int i2c_write_bytes(i2c_t dev, uint8_t address, const void *data, int length)
{
    SercomI2cm *i2c;

    switch (dev) {
#if I2C_0_EN
        case I2C_0:
            i2c = &I2C_0_DEV;
            break;
#endif
        default:
            return I2C_RETCODE_ERROR_BAD_DEV;
    }

    int ret = I2C_RETCODE_SUCCESS;

    // Start transaction and send slave address + read bit
    ret = _start( i2c, address, I2C_FLAG_WRITE );
    if( ret < 0 ){ return ret; }

    // Write data
    ret = _write( i2c, data, length );
    if( ret < 0 ){ return ret; }

    // End transaction
    _stop(i2c);

    return ret;
}

int i2c_write_byte(i2c_t dev, uint8_t address, uint8_t data)
{
    return i2c_write_bytes(dev, address, &data, 1);
}

int i2c_write_regs(i2c_t dev, uint8_t address, uint8_t reg, const void *data, int length)
{
    SercomI2cm *i2c;

    switch (dev) 
    {
#if I2C_0_EN
        case I2C_0:
            i2c = &I2C_0_DEV;
            break;
#endif
        default:
            return I2C_RETCODE_ERROR_BAD_DEV;
    }

    int ret = I2C_RETCODE_SUCCESS;

    // Start transaction and send slave address + read bit
    ret = _start( i2c, address, I2C_FLAG_WRITE );
    if( ret < 0 ){ return ret; }

    // Write register address/command code
    ret = _write( i2c, &reg, 1 );
    if( ret < 0 ){ return ret; }

    // Write data to register
    ret = _write( i2c, data, length );
    if( ret < 0 ){ return ret; }

    // End transaction
    _stop(i2c);

    return ret;
}

int i2c_write_reg(i2c_t dev, uint8_t address, uint8_t reg, uint8_t data)
{
    return i2c_write_regs(dev, address, reg, &data, 1);
}

i2c_retcode_t i2c_read_status_samd( i2c_t dev, SERCOM_I2CM_STATUS_Type* status )
{
    SercomI2cm *i2c;

    switch (dev) 
    {
#if I2C_0_EN
        case I2C_0:
            i2c = &I2C_0_DEV;
            break;
#endif
        default:
            return I2C_RETCODE_ERROR_BAD_DEV;
    }

    *status = (SERCOM_I2CM_STATUS_Type)( i2c->STATUS.reg );
    return I2C_RETCODE_SUCCESS;
}


// Power on
static void _i2c_poweron(SercomI2cm *sercom)
{
    if (sercom == NULL) 
    {
        return;
    }

    sercom->CTRLA.bit.ENABLE = 1;
    while (sercom->SYNCBUSY.bit.ENABLE) {}
}

// Power methods
void i2c_poweron(i2c_t dev)
{
    switch (dev) {
#if I2C_0_EN
        case I2C_0:
            _i2c_poweron(&I2C_0_DEV);
            break;
#endif
        default:
            return;
    }
}

static void _i2c_poweroff(SercomI2cm *sercom)
{
    if (sercom == NULL) {
        return;
    }
    sercom->CTRLA.bit.ENABLE = 0;
    SyncEnable_I2C( sercom );
}

void i2c_poweroff(i2c_t dev)
{
    switch (dev) {
#if I2C_0_EN
        case I2C_0:
            _i2c_poweroff(&I2C_0_DEV);
            break;
#endif
        default:
            return;
    }
}

static int _start(SercomI2cm *dev, uint8_t address, uint8_t rw_flag)
{
    DEBUG( "Starting I2C transaction\n" );
    uint32_t timeout_counter = 0;

    /* Wait for hardware module to sync */
    DEBUG("Wait for device to be ready\n");
    while(dev->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_MASK) {}

    /* Set action to ACK. */
    dev->CTRLB.reg &= ~SERCOM_I2CM_CTRLB_ACKACT;

    /* Send Start | Address | Write/Read */
    DEBUG("Generate start condition by sending address\n");
    dev->ADDR.reg = (address << 1) | rw_flag | (0 << SERCOM_I2CM_ADDR_HS_Pos);

    /* Wait for response on bus. */
    while (!(dev->INTFLAG.reg & SERCOM_I2CM_INTFLAG_MB) && !(dev->INTFLAG.reg & SERCOM_I2CM_INTFLAG_SB)) 
    {
        // At 48Mhz, this is at a minimum ~1ms, but is generally much longer due to execution of several other instructions
        if (++timeout_counter >= SAMD21_I2C_TIMEOUT) 
        {
            DEBUG("I2C_RETCODE_ERROR_TIMEOUT\n");
            return I2C_RETCODE_ERROR_TIMEOUT;
        }
    }

    /* Check for address response error unless previous error is detected. */
    /* Check for error and ignore bus-error; workaround for BUSSTATE
     * stuck in BUSY */
    if (dev->INTFLAG.reg & SERCOM_I2CM_INTFLAG_SB) 
    {
        /* Clear write interrupt flag */
        dev->INTFLAG.reg = SERCOM_I2CM_INTFLAG_SB;
        /* Check arbitration. */
        if (dev->STATUS.reg & SERCOM_I2CM_STATUS_ARBLOST) 
        {
            DEBUG("I2C_RETCODE_ERROR_ARBITRATION_LOST\n");
            return I2C_RETCODE_ERROR_ARBITRATION_LOST;
        }
    }
    /* Check that slave responded with ack. */
    else if (dev->STATUS.reg & SERCOM_I2CM_STATUS_RXNACK) 
    {
        /* Slave busy. Issue stop command. */
        SendBusCommand_I2C( dev, BUS_COMMAND_STOP );

        DEBUG("I2C_RETCODE_ERROR_NO_SLAVE_RESPONSE\n");
        return I2C_RETCODE_ERROR_NO_SLAVE_RESPONSE;
    }

    return I2C_RETCODE_SUCCESS;
}



static inline int _write(SercomI2cm *dev, const uint8_t *data, int length)
{
    DEBUG( "Starting I2C write\n" );

    uint16_t tmp_data_length = length;
    uint32_t timeout_counter = 0;
    uint16_t buffer_counter = 0;

    /* Write data buffer until the end. */
    DEBUG("Looping through bytes\n");
    while (tmp_data_length--) 
    {
        /* Check that bus ownership is not lost. */
        if ((dev->STATUS.reg & SERCOM_I2CM_STATUS_BUSSTATE_Msk) != BUSSTATE_OWNER) 
        {
            DEBUG("STATUS_ERR_PACKET_COLLISION\n");
            return I2C_RETCODE_ERROR_ARBITRATION_LOST;
        }

        /* Wait for hardware module to sync */
        while(dev->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_MASK) {}

        DEBUG("Written byte #%i to data reg, now waiting for DR to be empty again\n", buffer_counter);
        dev->DATA.reg = data[buffer_counter++];

        timeout_counter = 0;

        while (!(dev->INTFLAG.reg & SERCOM_I2CM_INTFLAG_MB) && !(dev->INTFLAG.reg & SERCOM_I2CM_INTFLAG_SB)) 
        {
            if (++timeout_counter >= SAMD21_I2C_TIMEOUT) 
            {
                LOG_DEBUG("STATUS_ERR_TIMEOUT\n");
                return I2C_RETCODE_ERROR_TIMEOUT;
            }
        }

        /* Check for NACK from slave. */
        if (dev->STATUS.reg & SERCOM_I2CM_STATUS_RXNACK) 
        {
            DEBUG("STATUS_ERR_OVERFLOW\n");
            return I2C_RETCODE_ERROR_NACK;
        }
    }

    return I2C_RETCODE_SUCCESS;
}

static inline int _read(SercomI2cm *dev, uint8_t *data, int length)
{
    DEBUG( "Starting I2C read\n" );
    uint32_t timeout_counter = 0;

    data[0] = dev->DATA.reg;

    for( uint8_t i = 1; i < length; ++i )
    {
        /* Set action to ack. */
        dev->CTRLB.reg &= ~SERCOM_I2CM_CTRLB_ACKACT;
        while( dev->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_SYSOP ){};

        SendBusCommand_I2C( dev, BUS_COMMAND_BYTE_READ );

        timeout_counter = 0;
        while( dev->INTFLAG.bit.SB == 0 )
        {
            // Waiting complete receive
            if (++timeout_counter >= SAMD21_I2C_TIMEOUT) 
            {
                // DEBUG("STATUS_ERR_TIMEOUT\n");
                return I2C_RETCODE_ERROR_TIMEOUT;
            }
        }

        /* Save data to buffer. */
        data[i] = dev->DATA.reg;
    }

    /* Send NACK before STOP */
    dev->CTRLB.reg |= SERCOM_I2CM_CTRLB_ACKACT;
    while( dev->SYNCBUSY.reg & SERCOM_I2CM_SYNCBUSY_SYSOP ){};

    return I2C_RETCODE_SUCCESS;
}

static inline void _stop(SercomI2cm *dev)
{
    SendBusCommand_I2C( dev, BUS_COMMAND_STOP );

    /* Wait for bus to be idle again */
    uint32_t timeout_counter = 0;
    while ((dev->STATUS.reg & SERCOM_I2CM_STATUS_BUSSTATE_Msk) != BUSSTATE_IDLE) 
    {
        if (++timeout_counter >= SAMD21_I2C_TIMEOUT) 
        {
            LOG_DEBUG("STATUS_ERR_TIMEOUT_STOP\n");
            return;
        }
    }
}

#endif
