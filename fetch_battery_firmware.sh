#!/bin/bash
set -ex

# Setup variables
# TODO: Add versioning information
VERSION="07b61d33"
BOARD_ID="000001-02"

#FIRMWARE_LOC="http://openrov-software-nightlies.s3-us-west-2.amazonaws.com/gitlab/amd64/bq34z100-firmware/bq34z100-firmware-$VERSION.tar.gz"

# Download the firmware tar
#wget $FIRMWARE_LOC

# Unzip
#tar xvf $(basename $FIRMWARE_LOC)

sudo apt-get update
sudo apt-get install bq34z100-firmware=2.0.1

# move the battery firmware into the right location
# TODO: Add logic for multiple boards
cp "/opt/openrov/firmware/bq34z100/openrov-trident-v2-fuel-gauge-firmware.h" "./openrov/libs/bq34z100_flasher/include/firmware/${BOARD_ID}-fuel-gauge-firmware.h" 

# Clean up
#rm $(basename $FIRMWARE_LOC)
#rm -rf "./$(basename $FIRMWARE_LOC .tar.gz)"
