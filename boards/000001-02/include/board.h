#ifndef BOARD_H_
#define BOARD_H_

#include "cpu.h"
#include "periph_conf.h"
#include "periph_cpu.h"

#ifdef __cplusplus
extern "C" {
#endif

// X-timer configuration
#define XTIMER              TIMER_0
#define XTIMER_CHAN         (0)

// GPIO pin configuration
#define I2C0_PWR_PIN        GPIO_PIN(PA, 11)
#define LED_MCU_0_PIN       GPIO_PIN(PA, 27)
#define LED_MCU_1_PIN       GPIO_PIN(PB, 23)
#define LED_MCU_2_PIN       GPIO_PIN(PB, 22)

// UART STDIO configuration
#define UART_STDIO_DEV          UART_DEV_DEBUG
#define UART_STDIO_RX_BUFSIZE   (512)
#define UART_STDIO_BAUDRATE     (115200)

void board_init(void);

#ifdef __cplusplus
}
#endif

#endif
