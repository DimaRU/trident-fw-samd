#include "board.h"
#include "periph/gpio.h"
#include "uart_stdio.h"

void board_init( void )
{
    // Initialize GPIO pins
    gpio_init( LED_MCU_0_PIN, GPIO_OUT );
    gpio_init( LED_MCU_1_PIN, GPIO_OUT );
    gpio_init( LED_MCU_2_PIN, GPIO_OUT );

    gpio_init( I2C0_PWR_PIN, GPIO_OUT );

    // Initialize the CPU and peripherals
    cpu_init();

    // Initialize the debug port
    uart_stdio_init();
}
