# Trident Build Notes

The build originally needed to happen against a specific version of GCC.  That is the version from 
Ubuntu Xenial (16.04.6 LTS), under which this was originally developed, however on Ubuntu
18.04, an older toolchain needs to be used.

You should use 18.04 for normal development purposes.

See notes at the bottom.


# Toolchain setup

```
sudo apt-get install libc6-i386

mkdir toolchain
cd toolchain
wget https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q3-update/+download/gcc-arm-none-eabi-4_9-2015q3-20150921-linux.tar.bz2
tar xfvj gcc-arm*.bz2
cd ..
```

# Build

Toolchain path:

```
export PREFIX=$PWD/toolchain/gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-
```

## Everything

./build_riot.sh

## Tailboard/SAMD only

To just build samd21 code:

./built_riot.sh trident

Verbose output:

QUIET=0 ./built_riot.sh trident


Firmware in:
    build/opt/openrov/firmware/000001-02/samd21/trident.bin
 
Flashing script on Trident:
    /opt/openrov/firmware/scripts/000001-02/flashSAMD.sh
    
## Fuel Gauge only


./build_riot.sh fuelgauge


Firmware in:
    build/opt/openrov/firmware/000001-02/samd21/fuelgauge.bin
 
Flashing script on Trident:
    /opt/openrov/firmware/scripts/000001-02/flashBatteryFlasher.sh


# Compiler notes 


The compatibility issues are because the firmware builds against arm-none-eabi-gcc cross
compiler in that version of Ubuntu, which is GCC version 4.9.3.

In Ubuntu 18.04 the GCC in arm-none-eabi-gcc cross compiler is 6.3.1.

It's unclear what goes wrong with this newer compiler, however these warnings
happen:


/usr/lib/gcc/arm-none-eabi/6.3.1/../../../arm-none-eabi/bin/ld: warning: /usr/lib/gcc/arm-none-eabi/6.3.1/../../../arm-none-eabi/lib/thumb/v6-m/libstdc++_nano.a(del_op.o) uses 2-byte wchar_t yet the output is to use 4-byte wchar_t; use of wchar_t values across objects may fail
/usr/lib/gcc/arm-none-eabi/6.3.1/../../../arm-none-eabi/bin/ld: warning: /usr/lib/gcc/arm-none-eabi/6.3.1/../../../arm-none-eabi/lib/thumb/v6-m/libstdc++_nano.a(new_op.o) uses 2-byte wchar_t yet the output is to use 4-byte wchar_t; use of wchar_t values across objects may fail
/usr/lib/gcc/arm-none-eabi/6.3.1/../../../arm-none-eabi/bin/ld: warning: /usr/lib/gcc/arm-none-eabi/6.3.1/../../../arm-none-eabi/lib/thumb/v6-m/libstdc++_nano.a(new_handler.o) uses 2-byte wchar_t yet the output is to use 4-byte wchar_t; use of wchar_t values across objects may fail

Forcing the build to use -fshort-char makes the problem worse
